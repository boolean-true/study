const {build} = require('esbuild')

;(async () => {
    await build({
        entryPoints: ['src/index.ts'],
        bundle: true,
        outfile: 'dist/index.js',
        watch: true
    })
})()