import PriorityQueue from "./priorityQueue";

const priorityQueue = new PriorityQueue<string>()

priorityQueue.enqueue('world', 1)
priorityQueue.enqueue('typescript', 3)
priorityQueue.enqueue('hello', 0)

console.log(priorityQueue.toString())