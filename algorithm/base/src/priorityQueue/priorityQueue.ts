interface PriorityQueueItem<T> {
    data: T,
    priority: number
}

class PriorityQueueNode<T> implements PriorityQueueItem<T> {
    data: T
    priority: number

    constructor(data: T, priority: number) {
        this.data = data
        this.priority = priority
    }
}

class PriorityQueue<T> {
    private readonly _repo: PriorityQueueItem<T>[]

    constructor() {
        this._repo = []
    }

    enqueue(data: T, priority: number) {
        const node = new PriorityQueueNode(data, priority)
        if (this.isEmpty() || (priority > this.last().priority)) {
            this._repo.push(node)
            return
        }

        if (priority < this.last().priority) {
            this._repo.unshift(node)
            return
        }

        for (let i = 0; i < this._repo.length; i++) {
            if (priority > this._repo[i].priority) {
                this._repo.splice(i, 0, node)
                return
            }
        }
    }

    dequeue(): PriorityQueueItem<T> | undefined {
        return this._repo.shift()
    }

    size(): number {
        return this._repo.length
    }

    isEmpty(): boolean {
        return this._repo.length === 0
    }

    first(): PriorityQueueItem<T> {
        return this._repo[0]
    }

    last(): PriorityQueueItem<T> {
        return this._repo[this._repo.length - 1]
    }

    toString(): string {
        return this._repo.reduce((pre, cur) => pre += `${cur.data} `, '')
    }
}

export default PriorityQueue