import Queue from "./queue";

// 击鼓传花
function passTheFlower<T>(group: T[], num: number) {
    const queue = new Queue<T>()
    for (let i = 0; i < group.length; i++) {
        queue.enqueue(group[i])
    }

    while (queue.size() > 1) {
        let j = 0
        while (j < num - 1) {
            const cur = queue.dequeue()
            if (cur) queue.enqueue(cur)
            j++
        }
        queue.dequeue()
    }

    return queue.dequeue()
}

const winner = passTheFlower<string>(['A', 'B', 'C', 'D', 'E'], 2)
console.log('winner is %s', winner)