class Queue<T> {
    private readonly _repo: T[]

    constructor() {
        this._repo = []
    }

    enqueue(v: T) {
        this._repo.push(v)
    }

    dequeue(): T | undefined {
        return this._repo.shift()
    }

    first(): T {
        return this._repo[0]
    }

    last(): T {
        return this._repo[this._repo.length - 1]
    }

    size(): number {
        return this._repo.length
    }

    isEmpty(): boolean {
        return this._repo.length === 0
    }

    toString(): string {
        return this._repo.join(' ')
    }
}

export default Queue