import Stack from "./stack";

// 利用 Stack 计算 二进制

function dec2bin(num: number) {
    const stack = new Stack<number>()
    while (num > 0) {
        stack.push(num % 2)
        num = Math.trunc(num / 2)
    }

    let bin = ''
    while (!stack.isEmpty()) {
        bin += stack.pop()
    }

    return bin
}

console.log(dec2bin(100))


