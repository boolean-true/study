class Stack<T> {
    private readonly _repo: T[]

    constructor() {
        this._repo = []
    }

    push(v: T) {
        this._repo.push(v)
    }

    pop(): T | undefined {
        return this._repo.pop()
    }

    peek(): T {
        return this._repo[this._repo.length - 1]
    }

    isEmpty(): boolean {
        return this._repo.length === 0
    }

    toString(): string {
        return this._repo.join(' ')
    }
}

export default Stack