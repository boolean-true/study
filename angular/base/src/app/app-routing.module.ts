import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BasicComponent} from "./pages/basic/basic.component";
import {EmptyComponent} from "./pages/empty/empty.component";
import {AboutComponent} from "./pages/about/about.component";
import {NewsComponent} from "./pages/news/news.component";
import {NewsHelloComponent} from "./pages/news-hello/news-hello.component";
import {NewsWorldComponent} from "./pages/news-world/news-world.component";
import {OutComponent} from "./pages/out/out.component";
import {OutLeftComponent} from "./pages/out-left/out-left.component";
import {OutRightComponent} from "./pages/out-right/out-right.component";
import {ForbiddenComponent} from "./pages/forbidden/forbidden.component";
import {CanActivateGuard} from "./guards/can-activate.guard";

const routes: Routes = [
  {
    path: 'basic',
    component: BasicComponent
  },
  {
    path: 'about/:id',
    component: AboutComponent
  },
  {
    path: 'news',
    component: NewsComponent,
    children: [
      {
        path: 'hello',
        component: NewsHelloComponent
      },
      {
        path: 'world',
        component: NewsWorldComponent
      },
      {
        path: '',
        redirectTo: 'hello',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'out',
    component: OutComponent,
    children: [
      {
        path: "left",
        component: OutLeftComponent,
        outlet: 'left'
      },
      {
        path: 'right',
        component: OutRightComponent,
        outlet: 'right'
      }
    ]
  },
  {
    path: 'lazy',
    loadChildren: () => import('./pages/lazy/lazy.module').then(m => m.LazyModule)
  },
  {
    path: 'resolver',
    loadChildren: () => import('./pages/before-resolve/before-resolve.module').then(m => m.BeforeResolveModule)
  },
  {
    path: 'can-activate',
    canActivate: [CanActivateGuard],
    loadChildren: () => import('./pages/can-activate/can-activate.module').then(m => m.CanActivateModule)
  },
  {
    path: 'can-activate-child',
    loadChildren: () => import('./pages/can-activate-child/can-activate-child.module').then(m => m.CanActivateChildModule)
  },
  {
    path: 'leave',
    loadChildren: () => import('./pages/leave/leave.module').then(m => m.LeaveModule)
  },
  {
    path: 'request',
    loadChildren: () => import('./pages/request/request.module').then(m => m.RequestModule)
  },
  {
    path: 'service',
    loadChildren: () => import('./pages/service/service.module').then(m => m.ServiceModule)
  },
  {
    path: 'forbidden',
    component: ForbiddenComponent
  },
  {
    path: '',
    redirectTo: 'basic',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: EmptyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
