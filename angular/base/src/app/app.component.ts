import {Component} from '@angular/core';
import {Router, NavigationEnd} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  path!: string

  constructor(
    private router: Router
  ) {
    router.events.pipe(
      filter((n: any) => n instanceof NavigationEnd))
      .subscribe((e: NavigationEnd) => {
        this.path = e.urlAfterRedirects
      })
  }
}
