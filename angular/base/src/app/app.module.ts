import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BasicComponent } from './pages/basic/basic.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { OutputPipe } from './pipes/output.pipe';
import { ChildComponent } from './pages/child/child.component';

import { FocusDirective } from './directives/focus.directive';
import { EmptyComponent } from './pages/empty/empty.component';
import { AboutComponent } from './pages/about/about.component';
import { NewsComponent } from './pages/news/news.component';
import { NewsHelloComponent } from './pages/news-hello/news-hello.component';
import { NewsWorldComponent } from './pages/news-world/news-world.component';
import { OutComponent } from './pages/out/out.component';
import { OutLeftComponent } from './pages/out-left/out-left.component';
import { OutRightComponent } from './pages/out-right/out-right.component';
import { ForbiddenComponent } from './pages/forbidden/forbidden.component';

import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import { AuthInterceptor } from './interceptor/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    BasicComponent,
    OutputPipe,
    ChildComponent,
    FocusDirective,
    EmptyComponent,
    AboutComponent,
    NewsComponent,
    NewsHelloComponent,
    NewsWorldComponent,
    OutComponent,
    OutLeftComponent,
    OutRightComponent,
    ForbiddenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
