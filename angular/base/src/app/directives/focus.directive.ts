import {AfterViewInit, Directive, ElementRef, HostListener, OnInit} from '@angular/core';

@Directive({
  selector: '[appFocus]'
})
export class FocusDirective implements AfterViewInit {

  $el!: HTMLInputElement
  constructor(private el: ElementRef) {
    this.$el = el.nativeElement
  }

  ngAfterViewInit() {
    this.$el.focus()
  }

  @HostListener('input', ['$event']) onInput(e: InputEvent) {
    console.log((<HTMLInputElement>e.target).value)
  }
}
