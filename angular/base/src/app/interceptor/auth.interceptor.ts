import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {finalize} from 'rxjs/operators'

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const _requset = request.clone({
      setHeaders: {
        hello: 'world'
      }
    })
    // return next.handle(_requset);

    // 响应拦截
    return next.handle(_requset).pipe(finalize(() => console.log('响应')))
  }
}
