import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Params, Router} from "@angular/router";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  query!: Params
  params!: Params

  toNewsWorld() {
    this.router.navigate(['/news/world'], {
      queryParams: {
        hello: 'world'
      }
    })
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {
    route.queryParams.subscribe((params: Params) => {
      this.query = params
    })

    route.params.subscribe((params: Params) => {
      this.params = params
    })
  }

  ngOnInit(): void {
  }
}
