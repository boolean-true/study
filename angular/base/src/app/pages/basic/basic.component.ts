import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, NgForm} from "@angular/forms";
import {FrameService} from "../../services/frame.service";

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit, AfterViewInit {
  title: string = 'basic'
  activeClass: string = 'active'
  active: boolean = true
  tabs: 'left' | 'right' = 'left'
  frames: string[] = ['Angular', 'React', 'Vue', 'Svelte']
  username: FormControl = new FormControl<string | null>(null)

  loginForm: FormGroup = new FormGroup<{
    username: FormControl<string | null>,
    password: FormControl<string | null>
  }>({
    username: new FormControl<string | null>(null),
    password: new FormControl<string | null>(null)
  })

  validateForm!: FormGroup

  framesData: string[] = []

  @ViewChild('paragraph') paragraph!: ElementRef<HTMLParagraphElement>
  @ViewChildren('items') items!: QueryList<HTMLLIElement>

  constructor(
    private fb: FormBuilder,
    private frameService: FrameService
  ) {
  }

  ngOnInit(): void {
    this.framesData = this.frameService.getFrames()
    this.handleInitValidateForm()
  }

  ngAfterViewInit() {
    console.log(`++ ngAfterViewInit ++`)
  }

  handleChangeActive() {
    this.active = !this.active
  }

  handleChangeTabs() {
    this.tabs = this.tabs === 'left' ? 'right' : 'left'
  }

  onBaseFormSubmit(form: NgForm) {
    console.log(form)
  }

  handleLogin() {
    console.log(this.loginForm.value)
  }

  handleInitValidateForm() {
    this.validateForm = this.fb.group({
      username: ['', [Validators.required, this.validatorUsername]],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(10)]]
    })
  }

  validatorUsername = (username: FormControl): { [key: string]: string } => {
    const {value = ''} = username
    if (!value) return {message: '请输入用户名'}
    const reg = /^[0-9]{11}$/
    if (reg.test(value)) return {}
    return {message: '用户名11位'}
  }

  handleSubmitValidateForm() {
    console.log(this.validateForm)
  }

  onAddSvelte(e: string) {
    this.frameService.pushFrames(e)
  }

  onPrintInput(e: HTMLInputElement) {
    console.log(e)
  }

  onPrintParagraph() {
    console.log(this.paragraph)
  }

  onPrintItems() {
    console.log(this.items.toArray())
  }
}
