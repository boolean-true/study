import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BeforeResolveComponent} from "./before-resolve.component";
import {GetBeforeNameResolver} from "../../resolver/get-before-name.resolver";

const routes: Routes = [{
  path: '',
  resolve: {
    payload: GetBeforeNameResolver
  },
  component: BeforeResolveComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BeforeResolveRoutingModule { }
