import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-before-resolve',
  templateUrl: './before-resolve.component.html',
  styleUrls: ['./before-resolve.component.scss']
})
export class BeforeResolveComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }


  ngOnInit(): void {
  console.log(this.route.snapshot.data?.['payload'])
  }

}
