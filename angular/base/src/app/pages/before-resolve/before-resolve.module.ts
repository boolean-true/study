import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BeforeResolveRoutingModule } from './before-resolve-routing.module';
import {BeforeResolveComponent} from "./before-resolve.component";


@NgModule({
  declarations: [BeforeResolveComponent],
  imports: [
    CommonModule,
    BeforeResolveRoutingModule
  ]
})
export class BeforeResolveModule { }
