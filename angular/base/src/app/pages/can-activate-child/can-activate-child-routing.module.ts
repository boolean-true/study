import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HelloComponent} from "./pages/hello/hello.component";
import {WorldComponent} from "./pages/world/world.component";
import {CanActivateChildComponent} from "./can-activate-child.component";
import {CanActivateChildGuard} from "../../guards/can-activate-child.guard";

const routes: Routes = [
  {
    path: '',
    canActivateChild: [CanActivateChildGuard],
    component: CanActivateChildComponent,
    children: [
      {
        path: 'hello',
        component: HelloComponent
      },
      {
        path: 'world',
        component: WorldComponent
      },
      {
        path: '',
        redirectTo: 'hello',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanActivateChildRoutingModule { }
