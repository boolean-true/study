import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CanActivateChildRoutingModule } from './can-activate-child-routing.module';
import { HelloComponent } from './pages/hello/hello.component';
import { WorldComponent } from './pages/world/world.component';
import { CanActivateChildComponent } from './can-activate-child.component';


@NgModule({
  declarations: [
    HelloComponent,
    WorldComponent,
    CanActivateChildComponent
  ],
  imports: [
    CommonModule,
    CanActivateChildRoutingModule
  ]
})
export class CanActivateChildModule { }
