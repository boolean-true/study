import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CanActivateComponent} from "./can-activate.component";

const routes: Routes = [
  {
    path: '',
    component: CanActivateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanActivateRoutingModule { }
