import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FrameService} from "../../services/frame.service";

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {
  framesData: string[] = []

  @Output() onAddSvelte = new EventEmitter<string>()

  constructor(private frameService: FrameService) {
  }

  ngOnInit(): void {
    this.framesData = this.frameService.getFrames()
  }

  handleAddSvelte() {
    this.onAddSvelte.emit('Svelte')
  }
}
