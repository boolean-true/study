import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LeaveComponent} from "./leave.component";
import {CanDeactivateGuard} from "../../guards/can-deactivate.guard";

const routes: Routes = [{
  path: '',
  canDeactivate: [CanDeactivateGuard],
  component: LeaveComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeaveRoutingModule { }
