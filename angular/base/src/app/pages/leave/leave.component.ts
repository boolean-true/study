import { Component, OnInit } from '@angular/core';
import {BeforeRouterLeave} from "../../guards/can-deactivate.guard";

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.scss']
})
export class LeaveComponent implements OnInit, BeforeRouterLeave {

  constructor() { }

  ngOnInit(): void {
  }

  beforeRouterLeave(): boolean {
    return false;
  }
}
