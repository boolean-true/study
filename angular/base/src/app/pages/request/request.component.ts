import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {finalize} from 'rxjs/operators'
export interface CatResponse {
  id: string
  url: string
  width: number
  height: number
}

export interface CatRequest {
  limit: number,
  size: 'full'
}

const catApi = 'https://api.thecatapi.com/v1/images/search'

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {

  catData!: CatResponse[]
  loading!: boolean

  constructor(private http: HttpClient) {
  }

  getCat(params?: CatRequest) {
    this.loading = true
    const vo = new HttpParams({fromObject: {...params}})
    this.http.get<CatResponse[]>(catApi, {
      params: vo
    }).pipe(finalize(() => this.loading = false)).subscribe({
      next: (data) => {
        this.catData = data
      },
      error: console.log
    })
  }

  handleGetCats() {
    this.getCat({limit: 9, size: 'full'})
  }

  ngOnInit(): void {
    this.getCat()
  }
}
