import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'output'
})
export class OutputPipe implements PipeTransform {
  transform(value: string): string {
    return `>>> ${value}`
  }
}
