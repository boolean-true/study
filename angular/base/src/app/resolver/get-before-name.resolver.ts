import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetBeforeNameResolver implements Resolve<string> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {
    return new Observable<string>((subscriber) => {
      console.log('Resolve start')
      setTimeout(() => {
        subscriber.next('hello')
        subscriber.complete()
        console.log('Resolve end')
      }, 1000)
    })
  }
}
