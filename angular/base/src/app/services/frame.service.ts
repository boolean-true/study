import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root' // 全局注入
})
export class FrameService {

  constructor() {
  }

  frames: string[] = ['Angular', 'React', 'Vue']

  getFrames = (): string[] => this.frames

  pushFrames = (frame: string): void => {
    this.frames.push(frame)
  }
}
