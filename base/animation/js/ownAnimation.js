function ownAnimation(element, config = {}, callback, delay = 10, coefficient = 10) {
  let oldValue = null;
  let speed = null;
  let flag = null;

  clearInterval(element.timer);
  element.timer = setInterval(() => {
    flag = true;
    for (let [key, value] of Object.entries(config)) {
      oldValue = Number.parseFloat(window.getComputedStyle(element, null)[key]);
      if (key === 'opacity') {
        value = value * 100;
        oldValue = oldValue * 100;
      }
      speed = (value - oldValue) / coefficient;
      speed = speed > 0 ? Math.ceil(speed) : Math.floor(speed);
      element.style[key] = key === 'opacity' ? (oldValue + speed) / 100 : `${oldValue + speed}px`;
      if (value !== oldValue) {
        flag = false
      }
    }
    if (flag) {
      clearInterval(element.timer);
      callback && callback()
    }
  }, delay);
}