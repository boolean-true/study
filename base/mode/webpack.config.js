const path = require("path")

const HtmlWebpackPlugin = require("html-webpack-plugin")

const resolve = (dir = "") => {
    return path.join(__dirname, dir)
}
module.exports = {
    entry: "./src/index.js",
    output: {
        path: resolve('dist'),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/, // 不需要转换的js文件
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'latest']
                    }
                }
            }
        ]
    },
    devServer: {
        contentBase: resolve('dist'),
        open: true,
        port: 8080
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./index.html"
        })
    ]
}
