const {app, BrowserWindow, Notification, ipcMain} = require('electron')

const config = {
    width: 300,
    height: 400,
    webPreferences: {
        nodeIntegration: true
    }
}

app.on('ready', async () => {
    const win = new BrowserWindow(config)
    handleIPC()
    await win.loadFile('./index.html')
})

function handleIPC() {
    ipcMain.handle('workNotification', async () => {
        return await new Promise((resolve, reject) => {
            const notification = new Notification({
                title: '时间到了',
                body: '是否开始休息',
                actions: [
                    {
                        text: '开始休息',
                        type: 'button'
                    }
                ],
                closeButtonText: '继续工作'
            })

            notification.show()

            notification.on('action', () => {
                resolve('rest')
            })

            notification.on('close', () => {
                resolve('work')
            })
        })
    })
}