const {ipcRenderer} = require('electron')
const Timer = require('timer.js')

const timerContainer = document.querySelector('#container')

function startWork() {
    const workTimer = new Timer({
        ontick: (ms) => updateTime(ms),
        onend: () => notification()
    })

    workTimer.start(70)
}

function updateTime(ms = 0) {
    const s = Number.parseInt(ms / 1000)
    const ss = `${s % 60}`
    const m = `${Math.floor((s / 60))}`
    timerContainer.innerHTML = `${m.padStart(2, '0')}:${ss.padStart(2, '0')}`
}

async function notification() {
    const result = await ipcRenderer.invoke('workNotification')
    const handle = {
        rest: () => {
            setTimeout(() => {
                alert('休息')
            }, 2 * 1000)
        },
        work: () => startWork()
    }

    handle[result]()
}

startWork()