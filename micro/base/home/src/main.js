import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

export let instance = null

export const render = () => {
    instance = new Vue({
        router,
        render: function (h) {
            return h(App)
        }
    }).$mount('#home')
    return instance
}


if (window.__POWERED_BY_QIANKUN__) {
    __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__
}

if (!window.__POWERED_BY_QIANKUN__) {
    render()
}


export const bootstrap = async () => {
    console.log('_home_created_')
}

export const mount = async () => {
    render()
    console.log('_home_mounted_')
}

export const unmount = async () => {
    console.log('_home_unmounted_')
}
