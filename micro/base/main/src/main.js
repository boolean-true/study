import {registerMicroApps, start} from 'qiankun'
import {createApp} from 'vue'
import App from './App.vue'
import router from './router'

registerMicroApps(
    [
        {
            name: 'vue2.x app',
            entry: '//localhost:2021',
            container: '#micro',
            activeRule: '/home',
        },
        {
            name: 'vue3.x app',
            entry: "//localhost:2022",
            container: '#micro',
            activeRule: '/profile',
        },
    ],
    {
        afterMount: [
            () => console.log('app mounted')
        ]
    }
)

start()
createApp(App).use(router).mount('#app')
