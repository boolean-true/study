import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

export let instance = null

const render = () => {
    instance = createApp(App).use(router).mount('#profile')
    return instance
}

if (window.__POWERED_BY_QIANKUN__) {
    __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__
}

if (!window.__POWERED_BY_QIANKUN__) {
    render()
}

export const bootstrap = async () => {
    console.log('_profile_created_')
}

export const mount = async () => {
    render()
    console.log('_profile_mounted_')
}

export const unmount = async () => {
    console.log('_profile_unmounted_')
}
