import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Demo',
    component: () => import('@/views/Demo')
  },
  {
    path: '/temp',
    name: 'Temp',
    component: () => import('../views/Temp.vue')
  }
]

const router = createRouter({
  history: createWebHistory(window.__POWERED_BY_QIANKUN__ ? '/profile' : '/'),
  routes
})

export default router
