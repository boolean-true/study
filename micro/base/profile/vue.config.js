const pkg = require('./package.json')
const {name} = pkg
const port = 2022

module.exports = {
    devServer: {
        port,
        disableHostCheck: true,
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    },
    configureWebpack: {
        output: {
            library: `${name}-[name]`,
            libraryTarget: 'umd',
            jsonpFunction: `webpackJsonp_${name}`
        }
    }
}
