const demo = require("./ware");
const app = demo();

app.use((req, res, next) => {
    console.log('请求开始');
    next(); // 放行
});

app.get('/index', (req, res, next) => {
    console.log(`${req.method}  ${req.url}`);
    res.end(`${req.method}  ${req.url}`);
    console.log('请求结束');
});

app.post('/foo', (req, res, next) => {
    console.log(`${req.method}  ${req.url}`);
    res.end(`${req.method}  ${req.url}`);
    console.log('请求结束');
});

app.listen(5000, () => {
    console.log('http://localhost:5000');
});