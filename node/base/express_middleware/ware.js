const http = require("http");

/* 模拟express 的中间件的机制 */

class Middleware {
    constructor() {
        // 定义仓库 用于存放中间件和对应的path    example => [{path: '/', dispose: [function]}, {path: '/api', dispose: [function, function]}]
        this.routers = {
            all: [], // 存放所有的中间件信息
            get: [], // 存放get中间件信息
            post: [], // 存放post中间件信息
        }
        // ... 其他注册方法类似
    };

    /* 初始化中间件的注册 */
    init(arg) {
        const item = {};
        if (typeof arg[0] === 'string') {
            // 如果传入的第一个参数是字符串，则表示传入了路径
            item.path = arg[0];
            item.dispose = arg.slice(1);
        } else {
            // 如果传入的第一个参数不是字符串，则表示未传入路径， 没传入路径时其本质就是作用在根路径上
            item.path = '/';
            item.dispose = arg;
        }

        return item;
    };

    use() {
        // 把用户通过use注册的中间件信息存入到all仓库中
        const allItem = this.init(Array.from(arguments));
        this.routers.all.push(allItem);
    };

    get() {
        // 把用户通过use注册的中间件信息存入到get仓库中
        const getItem = this.init(Array.from(arguments));
        this.routers.get.push(getItem);
    };

    post() {
        // 把用户通过post注册的中间件信息存入到post仓库中
        const postItem = this.init(Array.from(arguments));
        this.routers.post.push(postItem);
    };

    /* 根据请求方法找到中间件 */
    findWareItem(url, method) {
        // 用一个数组储存匹配到的中间键
        let temp = [];
        // 定义一个数组将当前需要用到的中间件信息仓库都存入 （all仓库 + 对应的请求方法仓库）
        const fullWare = [...this.routers.all, ...this.routers[method]];
        // 如果我们的请求路径中包含了中间键信息仓库的path，那么我们就把这个中间键存放到数组中并返回
        fullWare.forEach(item => {
            if (url.indexOf(item.path) === 0) {
                temp.push(...item.dispose);
            }
        });

        return temp;
    };

    /* next核心机制 */
    handleWare(req, res, currentWare) {
        function next() {
            // 获取当前请求中间件仓库的第一个中间件并从当前中间件仓库中将其删除
            const firstWareItem = currentWare.shift();
            if (firstWareItem) {
                /* ！！！执行中间件 */
                // 如果没有在中间件中执行 next 那那么下一个中间件就无法执行
                firstWareItem(req, res, next);
            }
        }

        // 先立即执行下自身next
        next();
    }

    serverHandle() {
        return (req, res) => {
            let {url, method} = req; // 直接从请求中拿到请求的方法和url
            method = method.toLowerCase();

            // 当请求来的时候我们需要根据method从用于中间件信息仓库中取出对应中间件 (直接忽略favicon.ico请求)
            const currentWare = url.includes("favicon.ico") ? [] : this.findWareItem(url, method);
            // 得到当前的请求的所有中间键后交给handleWare来处理next
            this.handleWare(req, res, currentWare);
        }
    };

    listen(...args) {
        const app = http.createServer(this.serverHandle()); // 这里只是为了模拟得到一个http服务
        app.listen(...args); // 直接交给node原生的http模块处理
    };
}

module.exports = () => {
    return new Middleware();
}