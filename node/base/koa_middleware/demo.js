const Demo = require("./ware");

const app = new Demo();

app.use(async (ctx, next) => {
    await next();
    console.log(`${ctx.method} ${ctx.url}`);
});

app.use(async ctx => {
    ctx.res.end("hello world")
});

app.listen(5000, () => {
    console.log("http://localhost:5000");
});