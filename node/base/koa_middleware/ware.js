const http = require('http');
const urlParser = require("url"); // 解析url字符串和url对象

class Middleware {
    constructor() {
        this.wares = [];  // 存储中间件
    };

    use(fun) {
        this.wares.push(fun); // 收集中间件
        return this;
    };

    /* 中间件处理的核心 */
    handleMiddleware(wareList) {
        return function (ctx) {
            // 中间件调用
            function dispose(index) {
                const currentFun = wareList[index];
                try {
                    // 使用Promise.resolve 包装 currentFun 防止外部传入的currentFun为一个普通函数
                    return Promise.resolve(
                        /* dispose.bind(null, index + 1)就是next 让dispose继续执行下一个中间件
                        如果没有在中间件中调用dispose.bind(null, index + 1) 则不会再去获取下一个中间件
                        */
                        currentFun(ctx, dispose.bind(null, index + 1))
                    );
                } catch (e) {
                    return Promise.reject(e);
                }
            }

            // 立即执行一下仓库的第一个中间件
            dispose(0);
        }
    };

    createContext(req, res) {
        const {method, url} = req;
        const {query} = urlParser.parse(url, true);

        // ... 这里远比这个复杂, 我们只做一个简单的包装
        return {
            method, url, query,
            res
        };
    }

    serverHandle() {
        return (req, res) => {
            // 当请求来的时候我们去触发中间件
            const fn = this.handleMiddleware(this.wares);
            // 得到当前请求的上下文对象
            const ctx = this.createContext(req, res);
            fn(ctx);
        }
    };

    listen(...args) {
        const app = http.createServer(this.serverHandle()); // 这里只是为了模拟得到一个http服务
        app.listen(...args); // 直接交给node原生的http模块处理
    };
}

module.exports = Middleware;