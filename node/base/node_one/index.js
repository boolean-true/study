const http = require("http"); // 网络服务模块
const fs = require("fs"); // 文件读取模块
const url = require("url"); // 解析url字符串和url对象
const querystring = require("querystring"); // 解析请求参数模块

const app = http.createServer(); // 创建一个http服务器


app.on("request", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  /*
  * req.method 获取请求方式
  * req.url 获取请完整路径
  * */
  // console.log(`${req.method} => ${req.url}`);

  if (req.method === "POST") {
    const {query, pathname} = url.parse(req.url, true); // 获取get请求参数 请求路径
    /*
    * 在post请求中参数是通过事件的方式来接收的, 参数不是一次就传输完成(Buffer)
    * data是请求参数传递时的时候发出的事件
    * end是请求参数传递完成后发出的事件
    * */
    let postParams = "";
    req.on(`data`, params => {
      postParams += params // 在data事件中拼接Buffer
    });
    req.on(`end`, () => {
      console.log()
      res.writeHead(200, {"Content-type": "application/json;charset=UTF-8"});
      console.log(postParams);
      const params = querystring.parse(postParams);
      res.end(`${JSON.stringify(params)}`); // 在end事件中获取post请求参数
    });
  } else {
    const {query, pathname} = url.parse(req.url, true); // 获取get请求参数 请求路径
    /* /set路由 */
    if (pathname === "/set") {

      /* 写文件 */
      fs.writeFile("./demo", query.msg, (err, data) => {
        res.writeHead(200, {"Content-type": "text/html;charset=UTF-8"});
        res.end(`<h1>${query.msg}</h1>`);
      });
    } else {
      /* 读文件 */
      fs.readFile('./demo', (err, data) => {
        res.writeHead(200, {"Content-type": "text/html;charset=UTF-8"});
        res.end(`<h1>${data}</h1>`);
      });
    }
  }

  // console.log(`==============`);
});

app.listen(3000, () => {
  console.log("http://localhost:3000");
});