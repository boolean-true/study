const redis = require("redis");

// 创建redis连接
const redisClient = redis.createClient("6379", "localhost");

// 监听redis错误
redisClient.on("error", error => {
    throw error;
});

// redis设置值
redisClient.set("name", "cxk", redis.print);

// redis获取值
redisClient.get("name", (error, result) => {
    if (error) {
        console.log(error);
        return;
    }
    console.log("result => ", result);

    // 断开
    redisClient.quit();
});