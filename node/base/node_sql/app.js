const mysql = require("mysql");

/* 创建连接 */
const con = mysql.createConnection({
   host: "localhost",
   port: "3306",
   user: "root",
   password: "root",
   database: "myblog"
});

/* 连接 */
con.connect();

/* sql语句 */
const selectTable = "select * from blogs";
const insertTableRow = "insert into users(`username`, `password`, `nickname`, `avatar`) values ('demo', '123456', '蔡徐坤', '008.png')";
const updateTableRow = "update users set nickname='蔡徐坤' where username='admin'";
const deleteTableRow = "delete from users where username='demo'";

/* 执行sql语句 */
con.query(selectTable, (error, result) => {
   if(error) {
       console.log("操作失败！");
       console.log(error);
       return;
   }
   console.log(result);
});

/* 关闭连接 */
con.end();

/*-------node环境变量-------*/
/*
使用cross-env来正确处理环境变量
在package.json 的 script 中配置执行脚本
"dev": "cross-env NODE_ENV=dev node ./app.js",
"prd": "cross-env NODE_ENV=prd node ./app.js"

使用 npm run dev
    npm run prd
    来在不同的环境下执行代码

使用process.env.NODE_ENV来获取当前环境下的环境变量
* */

console.log(process.env.NODE_ENV);
