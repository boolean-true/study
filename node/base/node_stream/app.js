// stream 标准输入输出
// process.stdin.pipe(process.stdout);

const fs = require("fs");
const path = require("path");

const file_A = path.resolve(__dirname, 'A.txt');
const file_B = path.resolve(__dirname, 'B.txt');

const streamA = fs.createReadStream(file_A); // 创建文件读取流
const streamB = fs.createWriteStream(file_B, {flags: 'a'}); // 创建文件写入流 // {flags: 'a'}代表追加不覆盖之前的

streamA.pipe(streamB); // 连接两个流

// 监听流的过程
streamA.on("data", chunk => {
    // console.log(chunk.toString());
});

// 监听传输完成
streamA.on("end", () => {
    console.log('done');
});