const fs = require('fs')
const path = require('path')
const http = require('http')
const Koa = require('koa')
const Router = require('koa-router')
const socket = require('socket.io')
const cors = require('koa2-cors')
const bodyparser = require('koa-bodyparser')

const app = new Koa
const router = new Router


app.use(cors())
app.use(bodyparser())

router.get('/', async (ctx) => {
    ctx.type = 'html'
    ctx.body = fs.createReadStream(path.resolve(__dirname, './socket.html'))
})

app.use(router.routes())

const server = http.createServer(app.callback())

const io = socket(server)

const handleIO = o => {
    o.on('chat', message => {
       io.emit('message', message)
    })
}

io.on('connection', handleIO)

server.listen(3000, 'localhost', function () {
    const host = this.address().address
    const port = this.address().port
    console.log(`http://%s:%s`, host, port)
})