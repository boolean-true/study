### package.json
```
{
  "dependencies": {
    "connect-redis": "^5.0.0", /* 连接redis */
    "cookie-parser": "~1.4.4", /* 处理cookie */
    "debug": "~2.6.9", 
    "express": "~4.16.1", 
    "express-session": "^1.17.1", /* 处理session */
    "http-errors": "~1.6.3", /* 处理错误请求 */
    "jade": "~1.11.0",
    "morgan": "~1.9.1", /* 写日志 */
    "mysql": "^2.18.1",
    "redis": "^3.0.2",
    "xss": "^1.0.8" /* xss处理 */
  }
}
```