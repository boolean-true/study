const createError = require('http-errors'); // 处理错误请求
const express = require('express');
const path = require('path');
const fs = require("fs");
const cookieParser = require('cookie-parser'); // 处理cookie
const logger = require('morgan'); // 写日志文件
const session = require('express-session');
const redisStore = require("connect-redis")(session); // 连接redis再将session放入

const redisClient = require("./db/redis"); // 导入redis初始化文件

const newRedis = new redisStore({
    client: redisClient // 创建redis对象
});

const logFile = path.join(__dirname, 'log', 'access.log');
const newWriteStream = file => {
    return fs.createWriteStream(file, {flags: 'a'});
}
/*
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
*/

// 导入路由
const blogRouter = require("./routes/blog");
const userRouter = require("./routes/user");

const app = express();

/*
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
*/

app.use(logger('dev', {
    stream: newWriteStream(logFile)
})); // 日志

app.use(express.json()); // 处理post数据格式为json
app.use(express.urlencoded({extended: false})); // 处理post数据不为json的其他格式
app.use(cookieParser()); // cookie 处理
app.use(session({
    secret: 'xxx', // 为session加盐
    cookie: {
        path: '/', // cookie生效路径
        httpOnly: true, //
        maxAge: 24 * 60 * 60 * 1000 // 设置cookie过期时间
    },
    store: newRedis // 将session存入redis
}));

/*
app.use(express.static(path.join(__dirname, 'public')));
*/

/*
app.use('/', indexRouter);
app.use('/users', usersRouter);
*/

// 注册路由
app.use('/api/blog', blogRouter);
app.use('/api/user', userRouter);

// 捕获404到错误处理
app.use(function (req, res, next) {
    next(createError(404));
});

// 处理错误
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'dev' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
