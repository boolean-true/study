const env = process.env.NODE_ENV;

let MYSQL_CONFIG = {};
let REDIS_CONFIG = {};

switch (env) {
    case "dev":
        /* 开发环境的数据库 */
        MYSQL_CONFIG = {
            host: "localhost",
            port: "3306",
            user: "root",
            password: "root",
            database: "myblog"
        };
        REDIS_CONFIG = {
            host: "localhost",
            port: "6379"
        };
        break;
    case "prd":
        /* 生产环境的数据库 */
        MYSQL_CONFIG = {
            host: "localhost",
            port: "3306",
            user: "root",
            password: "root",
            database: "myblog"
        };
        REDIS_CONFIG = {
            host: "localhost",
            port: "6379"
        };
        break;
    default:
        /* 默认 */
        MYSQL_CONFIG = {
            host: "localhost",
            port: "3306",
            user: "root",
            password: "root",
            database: "myblog"
        };
        REDIS_CONFIG = {
            host: "localhost",
            port: "6379"
        };
}
module.exports = {
    MYSQL_CONFIG,
    REDIS_CONFIG
}