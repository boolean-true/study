const {execSQL, escape} = require("../db/mysql");
const {execSAFE} = require("../utils/xss");
/* 博客列表 */
const blogList = (author, keyword) => {
    let sql = `select author, title, createtime, id from blogs where 1=1`;
    if (author) {
        sql += ` and author=${escape(author)}`;
    }
    if (keyword) {
        sql += ` and title like '%${keyword}%'`
    }
    sql += ` order by createtime desc`;
    return execSQL(sql);
}
/* 博客详情 */
const blogDetail = (id) => {
    let sql = `select * from blogs where id=${execSAFE(id)}`;
    return execSQL(sql).then(result => {
        return result[0]
    });
}
/* 新增博客 */
const newBlog = (blogData = {}) => {
    const {title, content, author} = blogData;
    const createtime = Date.now();
    let sql = `insert into blogs(title, content, author, createtime) values (${execSAFE(title)}, ${execSAFE(content)}, '${author}', '${createtime}')`;
    return execSQL(sql).then(result => {
        return {
            status: !!result.insertId,
            id: result.insertId
        }
    });
}

/* 编辑博客 */
const editBlog = (id, blogData = {}) => {
    const {title, content} = blogData;
    let sql = `update blogs set title=${execSAFE(title)}, content=${execSAFE(content)} where id=${execSAFE(id)}`;
    return execSQL(sql).then(result => {
        return {
            status: result.affectedRows > 0
        }
    })
}

/* 删除博客 */
const deleteBlog = (id, author) => {
    let sql = `delete from blogs where id=${execSAFE(id)} and author=${execSAFE(author)}`;
    return execSQL(sql).then(result => {
        return {
            status: result.affectedRows > 0
        }
    });
}

module.exports = {
    blogList,
    blogDetail,
    newBlog,
    editBlog,
    deleteBlog
}