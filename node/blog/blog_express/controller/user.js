const {execSQL} = require("../db/mysql");
const {execSAFE} = require("../utils/xss");
const {getSecret} = require("../utils/encry"); // 密码加密

const login = (username, password) => {
    let sql = `select username, nickname, avatar, id from users where username=${execSAFE(username)} and \`password\`='${getSecret(password)}'`;  // 加密后再比对密码
    return execSQL(sql).then(result => {
        const data = {
            status: !!result.length
        }
        if (result.length > 0) {
            const {username, nickname, avatar, id} = result[0];
            data.id = id;
            data.username = username;
            data.nickname = nickname;
            data.avatar = avatar;
        }
        return data;
    });
};

const register = (username, password, nickname, avatar) => {
    let checkSql = `select username from users where username=${execSAFE(username)}`;
    return execSQL(checkSql).then(res => {
        console.log(res.length);
        if (res.length) {
            return {status: false, msg: "该用户名已经被注册！"}
        }
        let newUserSql = `insert into users(username, \`password\`, nickname, avatar) values (${execSAFE(username)}, '${getSecret(password)}', ${execSAFE(nickname)}, ${execSAFE(avatar)})`;
        return execSQL(newUserSql).then(result => {
            return {
                status: !!result.insertId,
                msg: "注册成功",
                id: result.insertId
            }
        });
    });
}

const userInfo = (id) => {
    const sql = `select * from users where id='${id}'`;
    return execSQL(sql);
};

module.exports = {
    login,
    userInfo,
    register
}