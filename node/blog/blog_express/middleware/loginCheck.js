const {ErrorModel} = require("../model/resModel");

module.exports = (req, res, next) => {
    if (req.session.status) {
        next();
    } else {
        res.json(new ErrorModel({status: false, msg: "未登录"}, "未登录"));
    }
}