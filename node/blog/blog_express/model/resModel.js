/*
* @data为返回的数据
* @message为返会的提示信息
* */
class BaseModel {
    constructor(data, message) {
        // 兼容只传入message
        if (typeof data === 'string') {
            this.message = data;
            message = null;
            data = null;
        }
        
        if (data) {
            this.data = data;
        }

        if (message) {
            this.message = message;
        }
    }
}

class SuccessModel extends BaseModel {
    constructor(data, message) {
        super(data, message);
        this.code = 1; // 成功的状态码为1
    }
}

class ErrorModel extends BaseModel {
    constructor(data, message) {
        super(data, message);
        this.code = 0; // 失败的状态码为0
    }
}

module.exports = {
    SuccessModel,
    ErrorModel
}