const express = require("express");
const router = express.Router();

const {SuccessModel, ErrorModel} = require("../model/resModel");
const {blogList, blogDetail, editBlog, newBlog, deleteBlog} = require("../controller/blog");
const loginCheck = require("../middleware/loginCheck");

router.get("/list", (req, res, next) => {
    const {author = '', keyword = ''} = req.query;
    blogList(author, keyword).then(data => {
        res.json(new SuccessModel(data, '获取博客列表成功'));
    });
});

router.get("/detail", (req, res, next) => {
    const {id} = req.query;
    blogDetail(id).then(data => {
        res.json(new SuccessModel(data, "获取博客详情成功"));
    });
});

router.post("/edit", loginCheck, (req, res, next) => {
    const {id} = req.query;
    editBlog(id, req.body).then(data => {
        res.json(data.status ? new SuccessModel(data, "编辑博客成功") : new ErrorModel(data, "编辑博客失败"));
    });
});

router.post("/new", loginCheck, (req, res, next) => {
    req.body.author = req.session.username;
    newBlog(req.body).then(data => {
        res.json(new SuccessModel(data, "新增博客成功"));
    });
});

router.get("/delete", loginCheck, (req, res, next) => {
    const {id} = req.query;
    deleteBlog(id, req.session.username).then(data => {
        res.json(data.status ? new SuccessModel(data, "删除博客成功") : new ErrorModel(data, "删除博客失败"));
    });
});


module.exports = router;