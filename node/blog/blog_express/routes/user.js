const express = require("express");
const router = express.Router();

const {SuccessModel, ErrorModel} = require("../model/resModel");
const {login, register} = require("../controller/user");

router.post("/register", (req, res, next) => {
    const {username, password, nickname} = req.body;
    const avatar = `/img/avatar.jpg`;
    return register(username, password, nickname, avatar).then(data => {
        if (data) {
            res.json(new SuccessModel(data, '注册成功'));
        } else {
            res.json(new ErrorModel(data, '注册失败'));
        }
    });
});

router.post("/login", (req, res, next) => {
    const {username, password} = req.body;
    login(username, password).then(data => {
        if (data.status) {
            const {username, nickname, avatar, id} = data;
            req.session.id = id;
            req.session.username = username;
            req.session.nickname = nickname;
            req.session.avatar = avatar;
            req.session.status = true; // 标识
            /* 会自动同步到redis */
            res.json(new SuccessModel(data, '登录成功'));
        } else {
            res.json(new ErrorModel(data, '登录失败'));
        }
    });
});

router.get("/userInfo", (req, res, next) => {
    if (req.session.status) {
        const {username, nickname, avatar, status} = req.session;
        res.json(new SuccessModel({username, nickname, avatar, status}, '获取信息成功'));
    } else {
        res.json(new ErrorModel({status: false}, '未登录'));
    }
});

module.exports = router;