const crypto = require("crypto");
const salt = `demo`; // 盐

function md5(pass) {
    const md5 = crypto.createHash("md5");
    return md5.update(pass).digest("hex");
}

function getSecret(password) {
    return md5(`${salt}${password}${salt}`);
}

module.exports = {
    getSecret
}