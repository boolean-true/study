### package.json
```
{
    "dependencies": {
      "debug": "^4.1.1",
      "koa": "^2.7.0",
      "koa-bodyparser": "^4.2.1", // 处理post数据
      "koa-convert": "^1.2.0", // 处理post数据
      "koa-generic-session": "^2.0.4", // 如果中间件函数是一个生成器函数的话，会通过koa-convert将其转为async函数，再存到中间件数组。
      "koa-json": "^2.0.2", // JSON响应中间件
      "koa-logger": "^3.2.0", // 控制台格式输出
      "koa-morgan": "^1.0.1", // 日志
      "koa-onerror": "^4.1.0", // 处理错误
      "koa-redis": "^4.0.1", // redies
      "koa-router": "^7.4.0", // koa2的路由
      "koa-static": "^5.0.0", // koa2的静态托管
      "koa-views": "^6.2.0",// ssr渲染
      "mysql": "^2.18.1",
      "pug": "^2.0.3",
      "redis": "^3.0.2",
      "xss": "^1.0.8"
    }
}
```