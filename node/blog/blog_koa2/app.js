const Koa = require('koa'); //
const app = new Koa(); //
const views = require('koa-views'); // ssr渲染
const json = require('koa-json'); //
const onerror = require('koa-onerror'); // 处理错误
const bodyparser = require('koa-bodyparser'); // 处理post data
const logger = require('koa-logger'); // 控制台的输出
const session = require('koa-generic-session');
const redisStore = require('koa-redis');
const morgan = require('koa-morgan');

const fs = require('fs');
const path = require('path');

const {REDIS_CONFIG} = require('./config/db');

const newWriteStream = logFileName => {
    const logFile = path.join(__dirname, './', 'log', logFileName);
    return fs.createWriteStream(logFile, {flags: 'a'});
}

/*const index = require('./routes/index')
const users = require('./routes/users')*/
// 导入路由
const blog = require('./routes/blog');
const user = require('./routes/user');

// error handler
onerror(app);

// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}));
app.use(json());
app.use(logger());
// 记录日志
app.use(morgan('dev', {
    stream: newWriteStream('access.log') // 日志
}))

//
app.keys = ['demo']; // session 加盐
app.use(session({
    cookie: {
        path: '/',
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000
    },
    store: redisStore({
        all: `${REDIS_CONFIG.host}:${REDIS_CONFIG.port}` // redis配置
    })
}));

app.use(require('koa-static')(__dirname + '/public'));

/*app.use(views(__dirname + '/views', {
    extension: 'pug'
}))*/

// logger
app.use(async (ctx, next) => {
    const start = new Date();
    await next();
    const ms = new Date() - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
})

// routes
/*app.use(index.routes(), index.allowedMethods())
app.use(users.routes(), users.allowedMethods())*/
// 注册路由
app.use(blog.routes(), blog.allowedMethods());
app.use(user.routes(), user.allowedMethods());

// error-handling
app.on('error', (err, ctx) => {
    console.error('server error', err, ctx);
});

module.exports = app;
