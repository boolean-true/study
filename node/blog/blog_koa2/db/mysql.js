const mysql = require("mysql");

const {MYSQL_CONFIG} = require("../config/db");

const con = mysql.createConnection(MYSQL_CONFIG);

con.connect(function (error) {
    if(error) {
        throw error;
    }
});

function execSQL(sql) {
    return new Promise((resolve, reject) => {
        con.query(sql, (error, result) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(result);
        })
    })
}

module.exports = {
    execSQL,
    escape: mysql.escape
};