const {ErrorModel} = require("../model/resModel");

module.exports = async (ctx, next) => {
    if (ctx.session.status) {
        await next();
    } else {
        ctx.boyd = new ErrorModel({status: false, msg: "未登录"}, "未登录");
    }
}