const router = require("koa-router")();

const {SuccessModel, ErrorModel} = require("../model/resModel");
const {blogList, blogDetail, editBlog, newBlog, deleteBlog} = require("../controller/blog");
const loginCheck = require("../middleware/loginCheck");

router.prefix('/api/blog');

router.get('/list', async (ctx, next) => {
    const {author = '', keyword = ''} = ctx.query;
    const data = await blogList(author, keyword);
    ctx.body = new SuccessModel(data, '获取博客列表成功');
});


router.get("/detail", async (ctx, next) => {
    const {id} = ctx.query;
    console.log(id)
    const data = await blogDetail(id);
    ctx.body = new SuccessModel(data, "获取博客详情成功");
});

router.post("/edit", loginCheck, async (ctx, next) => {
    const {id} = ctx.query;
    const data = await editBlog(id, ctx.request.body);
    ctx.body = data.status ? new SuccessModel(data, "编辑博客成功") : new ErrorModel(data, "编辑博客失败");
});

router.post("/new", loginCheck, async (ctx, next) => {
    ctx.request.body.author = ctx.session.username;
    const data = await newBlog(ctx.request.body);
    ctx.body = new SuccessModel(data, "新增博客成功");
});

router.get("/delete", loginCheck, async (ctx, next) => {
    const {id} = ctx.query;
    const data = await deleteBlog(id, ctx.session.username);
    ctx.body = data.status ? new SuccessModel(data, "删除博客成功") : new ErrorModel(data, "删除博客失败");
});

module.exports = router;