const router = require("koa-router")();

const {SuccessModel, ErrorModel} = require("../model/resModel");
const {login, register} = require("../controller/user");

router.prefix('/api/user');

router.post('/login', async (ctx, next) => {
    const {username, password} = ctx.request.body;
    const data = await login(username, password);
    if (data.status) {
        const {username, nickname, avatar, id} = data;
        ctx.session.id = id;
        ctx.session.username = username;
        ctx.session.nickname = nickname;
        ctx.session.avatar = avatar;
        ctx.session.status = true; // 标识
        ctx.body = new SuccessModel(data, '登录成功');
    } else {
        ctx.body = new ErrorModel(data, '登录失败');
    }
});

router.post('/register', async (ctx, next) => {
    const {username, password, nickname} = ctx.request.body;
    const avatar = `/img/avatar.jpg`;
    const data = await register(username, password, nickname, avatar);
    if (data) {
        ctx.body = new SuccessModel(data, '注册成功');
    } else {
        ctx.body = new ErrorModel(data, '注册失败');
    }
});

router.get('/userInfo', async (ctx, next) => {
    if (ctx.session.status) {
        const {username, nickname, avatar, status} = ctx.session;
        ctx.body = new SuccessModel({username, nickname, avatar, status}, '获取信息成功');
    } else {
        ctx.body = new ErrorModel({status: false}, '未登录');
    }
});

module.exports = router;