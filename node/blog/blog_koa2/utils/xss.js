const {escape} = require("../db/mysql"); // sql防止注入
const xss = require("xss");

function execSAFE(str) {
    return xss(escape(str));
}

module.exports = {
    execSAFE
}