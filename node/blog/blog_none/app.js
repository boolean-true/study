const http = require("http");
const querystring = require("querystring");
const url = require("url"); // 解析url字符串和url对象

const {getPostData, getCookie, getCookieExpires} = require("./src/assets/js/utils");
const {setRedis, getRedis} = require("./src/db/redis");
const {writeAccessLog} = require("./src/utils/log");

const userHandle = require("./src/router/user");
const blogHandle = require("./src/router/blog");

const serverHandle = async (req, res) => {
    res.setHeader("Content-type", "application/json;charset=UTF-8");

    req.path = url.parse(req.url, true).pathname; // 获取请求路径
    req.query = querystring.parse(req.url.split("?")[1]); // 获取请求参数
    req.body = await getPostData(req); // 解析post参数
    req.cookie = getCookie(req.headers.cookie || ""); // 获取cookie
    req.sessionId = req.cookie.userid;
    let isSetCookie = false;
    if (req.sessionId) {
        const result = await getRedis(req.sessionId);
        if (!result) {
            setRedis(req.sessionId, {});
        }
        req.session = result;
    } else {
        isSetCookie = true;
        req.cookie.userid = `SESSION_${Date.now()}_${Math.floor(Math.random() * 10)}`;
        req.sessionId = req.cookie.userid;
        req.session = {};
        setRedis(req.sessionId, {});
    }

    if(!(req.path).includes('userInfo')) {
        // 写日志
        writeAccessLog('access.log',
            `${new Date().toLocaleString()}  ${req.method} ${req.path} ${JSON.stringify(req.headers['user-agent'])}`
        );
    }

    const blogResult = blogHandle(req, res);
    if (blogResult) {
        blogResult.then(blogData => {
            if (isSetCookie) {
                res.setHeader("Set-cookie", `userid=${req.cookie.userid};path=/; httpOnly; expires=${getCookieExpires()}`);
            }
            res.end(JSON.stringify(blogData));
        });
        return
    }

    const userData = userHandle(req, res);
    if (userData) {
        userData.then(userData => {
            if (isSetCookie) {
                res.setHeader("Set-cookie", `userid=${req.cookie.userid};path=/; httpOnly; expires=${getCookieExpires()}`);
            }
            res.end(JSON.stringify(userData));
        });
        return;
    }

    res.end("404 Not Found");
}

const server = http.createServer(serverHandle);

server.listen(8000, () => {
    console.log("server run in http://localhost:8000");
});