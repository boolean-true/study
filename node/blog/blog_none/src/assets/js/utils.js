const querystring = require("querystring"); // 解析请求参数模块

/* 获取post请求数据 */
function getPostData(req) {
    return new Promise((resolve, reject) => {
        // 如果请求方式不是post或者请求头不是json格式就直接返回
        if (req.method !== "POST" || !req.headers["content-type"] || !req.headers["content-type"].includes("json")) {
            resolve({});
            return;
        }
        let postData = "";
        req.on("data", params => {
            postData += params;
        });
        req.on("end", () => {
            if (!postData) {
                resolve({});
            } else {
                resolve(querystring.parse(postData));
            }
        });
    })
}

/* cookie */
function getCookie(cookieStr) {
    const arr = cookieStr.split(";");
    let cookie = {};
    arr.forEach(item => {
        if (!item) return;
        const temp = item.split("=");
        cookie[temp[0].trim()] = temp[1].trim();
    })
    return cookie;
}

/* 设置cookie过期 */
function getCookieExpires() {
    const date = new Date();
    date.setTime(date.getTime() + (24 * 3600 * 1000));
    return date.toGMTString();
}

module.exports = {
    getPostData,
    getCookie,
    getCookieExpires
}