const redis = require("redis");

const {REDIS_CONFIG} = require("../config/db");

const redisClient = redis.createClient(REDIS_CONFIG.port, REDIS_CONFIG.host);

redisClient.on("error", error => {
    throw error;
})

function setRedis(key, value) {
    if (typeof value === 'object') {
        value = JSON.stringify(value);
    }
    redisClient.set(key, value);
}

function getRedis(key) {
    return new Promise((resolve, reject) => {
        redisClient.get(key, (error, result) => {
            if (error) {
                reject(error);
            } else {
                /* 如果val为空则直接返回空 */
                if (Object.is(result, null)) {
                    resolve(null);
                } else {
                    /*如果val不为空则尝试将值转换为原格式， 如果不能使用JSON.parse转换就直接返回*/
                    try {
                        resolve(JSON.parse(result));
                    } catch (e) {
                        resolve(result);
                    }
                }
            }
        });
    })
}

module.exports = {
    setRedis,
    getRedis
};