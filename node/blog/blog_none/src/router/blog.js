const {SuccessModel, ErrorModel} = require("../model/resModel");
const {blogList, blogDetail, newBlog, editBlog, deleteBlog} = require("../controller/blog");

// 登录验证 /* 如果没登陆就直接返回 */
function checkNoLogin(req) {
    if (!req.session.status) {
        return Promise.resolve(new ErrorModel({status: false}, '没登陆'));
    }
}

const blogHandle = (req, res) => {
    const type = req.method; /* 请求方法 */
    const id = req.query.id || ""

    if (type === "GET") {
        /*GET*/
        if (req.path === "/api/blog/list") {
            const author = req.query.author || "";
            const keyword = req.query.keyword || "";
            return blogList(author, keyword).then(data => {
                return new SuccessModel(data, "获取博客列表成功");
            });
        } else if (req.path === "/api/blog/detail") {
            return blogDetail(id).then(data => {
                return new SuccessModel(data, "获取博客详情成功");
            })
        } else if (req.path === "/api/blog/delete") {
            /* 检测是否登录 */
            const NoLogin = checkNoLogin(req);
            // 未登录
            if (NoLogin) return NoLogin;
            req.body.author = req.session.username;
            return deleteBlog(id, req.body.author).then(data => {
                return data.status ? new SuccessModel(data, "删除博客成功") : new ErrorModel(data, "删除博客失败");
            });
        }
    } else if (type === "POST") {
        /*POST*/
        if (req.path === "/api/blog/new") {
            /* 检测是否登录 */
            const NoLogin = checkNoLogin(req);
            // 未登录
            if (NoLogin) return NoLogin;

            req.body.author = req.session.username;
            return newBlog(req.body).then(data => {
                return new SuccessModel(data, "新增博客成功");
            });
        } else if (req.path === "/api/blog/edit") {
            /* 检测是否登录 */
            const NoLogin = checkNoLogin(req);
            // 未登录
            if (NoLogin) return NoLogin;

            return editBlog(id, req.body).then(data => {
                return data.status ? new SuccessModel(data, "编辑博客成功") : new ErrorModel(data, "编辑博客失败");
            });
        }
    } else {
        return {
            error: "请求类型错误"
        }
    }

}

module.exports = blogHandle;