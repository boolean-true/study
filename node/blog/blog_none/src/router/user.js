const {SuccessModel, ErrorModel} = require("../model/resModel");
const {setRedis, getRedis} = require("../db/redis");

const {login, register} = require("../controller/user");

// 登录验证 /* 如果没登陆就直接返回 */
function checkNoLogin(req) {
    if (!req.session.status) {
        return Promise.resolve(new ErrorModel({status: false}, '没登陆'));
    }
}

const userHandle = (req, res) => {
    const type = req.method; /* 请求方法 */

    if (type === "GET") {
        /*GET*/
        if (req.path === "/api/user/userInfo") {
            /* 检测是否登录 */
            const NoLogin = checkNoLogin(req);
            // 未登录
            if (NoLogin) return NoLogin;

            return Promise.resolve(new SuccessModel(req.session, '获取用户信息成功'));
        }
    } else if (type === "POST") {
        /*POST*/
        if (req.path === "/api/user/login") {
            const {username, password} = req.body;
            return login(username, password).then(data => {
                if (data.status) {
                    const {username, nickname, avatar, id} = data;
                    req.session.id = username;
                    req.session.username = username;
                    req.session.nickname = nickname;
                    req.session.avatar = avatar;
                    req.session.status = true; // 标识
                    setRedis(req.sessionId, {username, nickname, avatar, id, status: true});
                    return new SuccessModel(data, '登录成功');
                } else {
                    return new ErrorModel(data, '登录失败');
                }
            });
        }
        if (req.path === "/api/user/register") {
            const {username, password, nickname} = req.body;
            const avatar = `/img/avatar.jpg`;
            return register(username, password, nickname, avatar).then(data => {
                return new SuccessModel(data, '注册成功');
            });
        }
    } else {
        return {
            error: "请求类型错误"
        }
    }

}

module.exports = userHandle;