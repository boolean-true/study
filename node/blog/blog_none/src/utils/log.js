const fs = require("fs");
const path = require("path");

// 创建写文件流
function newWriteStream(fileName) {
    const filePath = path.join(__dirname, '../', '../', 'log', fileName);
    return fs.createWriteStream(filePath, {flags: 'a'});      // {flags: 'a'}代表追加
}

// 将日志写入
function writeLog(stream, log) {
    stream.write(log);
}

// 写access日志
function writeAccessLog(filename, log) {
    const writeStream = newWriteStream(filename);
    writeLog(writeStream, log + '\n');
}

module.exports = {
    writeAccessLog
}


