const express = require('express');
const fs = require('fs');
const path = require("path");
const cors = require('cors');

const app = express();
const staticVideo = path.join(__dirname, '../', '../', 'video');
// app.use(express.static('static'));
app.use(cors());

app.get('/video', function (req, res, next) {
    let path = `${staticVideo}/young.flv`;
    let stat = fs.statSync(path);
    let fileSize = stat.size;
    let range = req.headers.range;

    // fileSize 3332038

    if (range) {
        let parts = range.replace(/bytes=/, "").split("-");
        let start = parseInt(parts[0], 10);
        let end = parts[1] ? parseInt(parts[1], 10) : start + 999999;

        // end 在最后取值为 fileSize - 1 
        end = end > fileSize - 1 ? fileSize - 1 : end;

        let chunksize = (end - start) + 1;
        let file = fs.createReadStream(path, {start, end});
        let head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/x-flv',
        };
        res.writeHead(200, head);
        file.pipe(res);
    } else {
        let head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/x-flv',
        };
        res.writeHead(200, head);
        fs.createReadStream(path).pipe(res);
    }

});

app.listen(8080, () => {
    console.log('running.......');
});
