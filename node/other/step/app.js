const fs = require('fs');
const path = require('path');
const express = require('express');
const axios = require('axios');
const md5 = require('md5');
const nodemailer = require("nodemailer");

const app = express();
const transporter = nodemailer.createTransport({
  service: 'qq',
  host: 'smtp.qq.com',
  auth: {
    user: '3025822868@qq.com',
    pass: 'dnsmbaoquuqnddcf',
  },
});
const autoTimerConfig = {
  interval: 1, //间隔天数，间隔为整数
  runNow: true, //是否立即运行
  time: "00:01:00" //执行的时间点 时在0~23之间
}
const BASE_URL = `http://bushu99.com`

const dateFormat = (date, format) => {
  if (typeof date === 'string') {
    let mts = date.match(/(\/Date\((\d+)\)\/)/)
    if (mts && mts.length >= 3) {
      date = parseInt(mts[2])
    }
  }
  date = new Date(date)
  if (!date || date.toUTCString() === 'Invalid Date') {
    return ''
  }
  let map = {
    'M': date.getMonth() + 1, // 月份
    'd': date.getDate(), // 日
    'h': date.getHours(), // 小时
    'm': date.getMinutes(), // 分
    's': date.getSeconds(), // 秒
    'q': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds() // 毫秒
  }
  format = format.replace(/([yMdhmsqS])+/g, function (all, t) {
    let v = map[t]
    if (v !== undefined) {
      if (all.length > 1) {
        v = '0' + v
        v = v.substr(v.length - 2)
      }
      return v
    } else if (t === 'y') {
      return (date.getFullYear() + '').substr(4 - all.length)
    }
    return all
  })
  return format
};

const autoTimer = (config, fun) => {
  config.time = config.time || '00:00:00'
  config.interval = config.interval || 1
  config.runNow && fun()

  const nowTime = new Date().getTime()
  const timePoints = config.time.split(':').map(i => Number.parseInt(i))
  let recent = new Date().setHours(...timePoints)
  recent = nowTime > recent ? recent + 24 * 3600000 : recent
  setTimeout(() => {
    fun()
    setInterval(fun, config.interval * 3600000 * 24)
  }, recent - nowTime)
};

const ownReadFile = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.resolve(fileName), 'utf-8', (error, result) => {
      if (error) {
        reject(error)
      } else {
        resolve(JSON.parse(result))
      }
    })
  })
}

const cookieToString = (cookie) => {
  let str = '';
  for (let [key, value] of Object.entries(cookie)) {
    str += `${key}=${value};`
  }
  return str
};

const login = async (username = '', password = '') => {
  const loginParmas = {
    appType: "6",
    clientId: "52569c045dc348f12dfc4c85000ad832",
    loginName: username,
    password: md5(password),
    timestamp: Date.now()
  };
  const userInfo = {};

  const {headers, data} = await axios.post(`${BASE_URL}/Login?systemType=1&version=4.6.7`, loginParmas);
  const setCookie = headers['set-cookie'];
  const {userId} = data.data;

  setCookie.forEach(item => {
    const [cookieStr] = item.split(';')
    const [key, value] = cookieStr.split('=');
    if (!userInfo.hasOwnProperty(key)) {
      userInfo[key] = value;
    }
  });

  return Object.assign(userInfo, {
    phoneNum: username,
    password: password,
    userId: userId
  })
};

const update = async (cookie, step = "98800") => {
  const nowDate = new Date();
  const updateParmas = {
    list: [
      {
        calories: 2470,
        created: nowDate.getTime(),
        dataSource: "3",
        deviceId: "M_NULL",
        measurementTime: dateFormat(nowDate, 'yyyy-MM-dd hh:mm:ss'),
        priority: "0",
        step: step,
        systemType: "1",
        type: "0",
        distance: 59280,
        userId: cookie.userId,
        version: "4.6.7"
      }
    ],
    timestamp: nowDate
  };
  const {data: result} = await axios.post(`${BASE_URL}/Update`, updateParmas,
    {
      headers: {
        cookie: cookieToString(cookie)
      }
    });

  return result;
};

const sendMail = (config, callback) => {
  const {title = '', content = '', text = '', toMail = '', fromMail = ''} = config
  transporter.sendMail({
    from: fromMail,
    to: toMail,
    subject: title,
    text: text,
    html: content
  }, () => {
    callback && callback();
  });
};

app.listen(5000, () => {
  autoTimer(autoTimerConfig, async () => {
    const clientData = await ownReadFile('client.json');
    clientData.forEach(item => {
      const {username, password, mail} = item;
      login(username, password).then(cookie => {
        update(cookie).then((result) => {
          sendMail({
            title: '刷步成功！',
            content: `<code>${JSON.stringify(result)}</code>`,
            text: '',
            toMail: mail,
            fromMail: '"👻" <3025822868@qq.com>'
          });
          console.log(`${new Date().toLocaleString()} ${username} 成功`)
        })
      });
    });
  });
  console.log('run 5000')
})