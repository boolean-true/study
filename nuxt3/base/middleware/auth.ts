export default defineNuxtRouteMiddleware((to, from) => {
    console.log('auth', to)
    if (to.path === '/auth') {
        return navigateTo('/401')
    }
});
