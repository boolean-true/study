interface HelloBody {
    name: string
}

interface HelloQuery {
    id: string
}

export default defineEventHandler(async (event) => {
    const body = await useBody<HelloBody>(event)
    const query = await useQuery(event)
    const cookie = await useCookies(event)
    return {
        body,
        query,
        cookie,
        type: 'post',
        message: 'hello world'
    }
})