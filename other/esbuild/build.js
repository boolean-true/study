const {build} = require('esbuild')

;(async () => {
    await build({
        entryPoints: ['index.ts'],
        bundle: true,
        outfile: 'index.js',
        watch: true,
        format: 'esm',
        minify: true
    })
})()