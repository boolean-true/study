const {build} = require('esbuild')
const vue = require('esbuild-vue')
const path = require('path')
const outDir = path.resolve(__dirname, '../../../../vue2/temp/src/lib')

;(async () => {
    const time = 'build'
    console.time(time)
    await build({
        entryPoints: ['index.js'],
        bundle: true,
        outdir: outDir,
        resolveExtensions: ['.vue', '.js'],
        plugins: [
            vue({
                extractCss: true
            })
        ],
        format: 'esm',
        // minify: true
    })
    console.timeEnd(time)
    console.log('bundle in %s', outDir)
})()
