import DInput from './src'

export default {
    install: Vue => {
        Vue.component(DInput.name, DInput)
    }
}