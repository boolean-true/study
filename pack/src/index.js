require('./css/index.css');
require('./less/index.less');
require('./sass/index.scss');
const { sum, Father, Son } = require('./js/tool');
console.log(`调用了tool.js中的sum方法，结果为 => ${sum(1, 1)}`);

const father_one = new Father(1, 2); // 构造了一个Father的实例
const son_one = new Son(2, 4);

console.log(father_one);
father_one.father_play(); // Father的实例father_one执行了father_play方法
const res_one =  father_one.father_sum();
console.log(res_one);

console.log('******************************');

console.log(son_one);
son_one.father_play(); // 子类继承了父类的father_play方法
const res_two = son_one.father_sum();
console.log(res_two);
const res_three = son_one.son_sub();
console.log(res_three);


/* **************Vue************** */ 
console.log('******************************');

import Vue from 'vue';
import App from './js/app'

const app = new Vue({
    el: '#app',
    template: '<App/>',
    components: {
        App
    }
});