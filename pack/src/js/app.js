import demo from '../view/demo.vue'

// 抽离的第二步
export default {
    template: `<div>
                  <h1>{{msg}}</h1>
                  <demo></demo>
               </div>`,
    components: {
        demo
    },
    data() {
        return {
            msg: 'hello'
        }
    },
    mounted() {
        console.log(this.msg)
    }
}