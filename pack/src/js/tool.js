function sum(a, b) {
    return a + b
};

// 类
class Father {
    constructor(a, b) {
        this.a = a;
        this.b = b;
    }
    father_play() {
        console.log('Father的father_play方法');
    }
    father_sum() {
       return this.a + this.b
    }
}

// 继承
class Son extends Father {
    constructor(a, b) {
        super(a, b); // super关键字 将子类的参数传递给父类
        this.a = a;
        this.b = b;
    }
    son_sub() {
        return this.a - this.b;
    }
}

module.exports = {
    sum,
    Father,
    Son
}