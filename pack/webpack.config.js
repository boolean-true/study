const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  entry: './src/index.js', // 入口文件
  output: {
    path: path.join(__dirname, 'dist'), // 打包后的文件存放位置
    filename: 'bundle.js', // 打包后的文件名
    // publicPath: 'dist/' // 设置静态资源的路径
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        /* style-loader只负责将css样式载入到dom中 css-loader只负责对css文件进行加载 */
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }]
      }, // npm install --save-dev css-loader style-loader
      {
        test: /\.less$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'less-loader' }]
      }, // npm install --save-dev less-loader less
      {
        test: /\.scss$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'sass-loader' }]
      }, // npm install sass-loader node-sass --save-dev
      {
        test: /\.(png|jpg|gif)$/,
        use: [{
            loader: 'url-loader',
            options: {
              limit: 10240,// limit为最小字节，小于limit的图片文件会被转换成base64，大于的会启用file-loader进行打包
              name: 'img/[name].[hash:8].[ext]' // 设置图片的打包位置， [代表原文件的文件名].[hash值:多少位的hash].[文件扩展名]
            }
          }]
      }, // npm install --save-dev url-loader file-loader
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/, // 不需要转换的js文件
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015'] 
          }
        }
      }, // es6转低级es // npm install --save-dev babel-loader@7 babel-core babel-preset-es2015
        {
          test: /\.vue$/,
          use: [{loader: 'vue-loader'}]
        } // 编译vue文件 // npm install --save-dev  vue-template-compiler vue-style-loader vue-loader
    ]
  },
  resolve: {
    extensions:['.js', '.jsx', '.json', '.vue'],//这几个后缀名的文件后缀可以省略不写
    alias: {
      'vue$': 'vue/dist/vue.esm.js' 
    }  // 指定vue的文件
  },
  plugins: [
    new VueLoaderPlugin(), // vueloader 插件
    new webpack.BannerPlugin('版权所有，翻版必究'), // 打包后的声明banner插件
    new htmlWebpackPlugin({
      template: 'index.html' // 指定文件模板
    }) // 在dist文件夹下生成入口html文件
  ],
  devServer: {
    contentBase: './dist', // dev serve 的托管文件夹
    inline: true // 开启实时监听
  }
}   