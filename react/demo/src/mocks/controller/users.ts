import db from "../db"
import type {IndexableType} from 'dexie'
import awaitWrapper from "../../utils/awaitWrapper";

export interface User {
    username: string,
    password?: string,
    readonly id: IndexableType,
    token?: string
}

const userTable = db.table<User>('users')

const genToken = (id: IndexableType, len: number = 32): string => {
    const chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz23456789'
    let token = `${id.toString()}_`
    for (let i = 0; i < len; ++i) {
        token += chars.charAt(Math.floor(Math.random() * chars.length))
    }
    return btoa(token)
}

const getUsers = (): Promise<User[]> => {
    return userTable.toArray()
}

const getUserByUserName = async (username: User['username']): Promise<User> => {
    const result = await userTable.where('username').equals(username).toArray()
    if (!result || !result.length) return Promise.reject(new Error('账户不存在'))
    const [data] = result
    return data
}

const updateToken = async (id: User['id'], token: User['token']) => {
    return userTable.update(id, {token})
}

const createUser = async (user: User): Promise<User> => {
    const [error, id] = await awaitWrapper(userTable.add(user))
    if (error) return Promise.reject(new Error('新增失败'))
    const result = await userTable.where('id').equals(id).toArray()
    if (!result || !result.length) return Promise.reject(new Error('新增失败'))
    const token = genToken(id)
    await updateToken(id, token)
    return Promise.resolve({username: user.username, id, token})
}

const userLogin = async (username: User['username'], password: User['password']) => {
    const [userError, user] = await awaitWrapper(getUserByUserName(username))
    if (userError || !user) return Promise.reject(userError)
    const {password: _password} = user
    if (password !== _password) {
        return Promise.reject(new Error('请检查用户名或密码'))
    }
    const token = genToken(user.id)
    await updateToken(user.id, token)
    return Promise.resolve({username: user.username, id: user.id, token})
}

export {
    getUsers,
    createUser,
    getUserByUserName,
    userLogin
}