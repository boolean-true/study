import Dexie from 'dexie'
import {DB_NAME, DB_VERSION} from "./config"

const db = new Dexie(DB_NAME)

db.version(DB_VERSION).stores({
    users: '++id, &username,password,token', // users 表
})

export default db