import {setupWorker} from 'msw'
import handlers from './routes'
import db from "./db";

const setupMock = () => db.open().then(() => setupWorker(...handlers).start())

export default setupMock