export interface BaseModelType<T> {
    message: string,
    data: T,
    code: number
}

export interface SuccessModelType<T> extends BaseModelType<T> {
    code: 200
}

export interface FailModelType<T> extends BaseModelType<T> {
    code: 500
}

export interface ErrorModelType<T> extends BaseModelType<T> {
    code: 400
}

class BaseModel<T> implements BaseModelType<T> {
    message: string
    data: any
    code: number

    constructor(data: T, message: string, code: number) {
        this.message = message
        this.data = data
        this.code = code
    }
}

class SuccessModel<T> extends BaseModel<T> implements SuccessModelType<T> {
    code: 200

    constructor(data: T, message: string = '成功') {
        const code = 200
        super(data, message, code)
        this.code = code
    }
}

class FailModel<T> extends BaseModel<T> implements FailModelType<T> {
    code: 500

    constructor(message: string = '失败', data: T) {
        const code = 500
        super(data, message, code)
        this.code = code
    }
}

class ErrorModel<T> extends BaseModel<T> implements ErrorModelType<T> {
    code: 400

    constructor(message: string = '错误', data: T) {
        const code = 400
        super(data, message, code)
        this.code = code
    }
}

export {
    SuccessModel,
    FailModel,
    ErrorModel
}