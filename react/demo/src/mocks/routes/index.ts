import {RequestHandler} from "msw";

const allModules = import.meta.globEager('./*')
type Handles = RequestHandler[]

const modules: Handles = []
Object.keys(allModules).forEach(key => {
    modules.push(...allModules[key].default)
})

export default modules