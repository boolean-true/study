import {rest} from "msw";
import type {RequestHandler} from "msw";
import {ErrorModel, FailModel, SuccessModel} from "../model/responseModel";
import {getUsers, createUser, userLogin} from "../controller/users";

import type {User} from "../controller/users";
import awaitWrapper from "../../utils/awaitWrapper";
import parseReqBody from "../utils/parseReqBody";

type Handles = RequestHandler[]

const user: Handles = [
    rest.get('/api/users', async (req, res, context) => {
        const users = await getUsers()
        return res(
            context.status(200),
            context.json(new SuccessModel<User[]>(users))
        )
    }),

    rest.post('/api/register', async (req, res, context) => {
        const [bodyError, user] = parseReqBody<User>(req)
        if (bodyError || !user) {
            return res(
                context.status(200),
                context.json(new ErrorModel(bodyError?.message, null))
            )
        }

        const [error, data] = await awaitWrapper(createUser(user))
        if (error) {
            return res(
                context.status(200),
                context.json(new FailModel('注册失败，账户重复', null))
            )
        }
        return res(
            context.status(200),
            context.json(new SuccessModel(data, '注册成功'))
        )
    }),
    rest.post('/api/login', async (req, res, context) => {
        const [bodyError, body] = parseReqBody<User>(req)
        if (bodyError || !body) {
            return res(
                context.status(200),
                context.json(new ErrorModel('请输入账户名和密码', null))
            )
        }
        const [userError, user] = await awaitWrapper(userLogin(body.username, body.password))
        if (userError) {
            return res(
                context.status(200),
                context.json(new ErrorModel(userError.message, null))
            )
        }

        return res(
            context.status(200),
            context.json(new SuccessModel(user, '登录成功'))
        )
    })
]

export default user