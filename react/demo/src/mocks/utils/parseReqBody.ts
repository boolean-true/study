import {RestRequest} from "msw";

const parseReqBody = <T>(req: RestRequest): ([null, T | null] | [Error, null]) => {
    const {body, bodyUsed} = req
    if (!bodyUsed || typeof body !== 'string') {
        return [new Error('请检查参数'), null]
    }
    let data: T | null = null
    try {
        data = JSON.parse(body)
    } catch (e) {
        console.log(e)
    }
    return [null, data]
}
export default parseReqBody