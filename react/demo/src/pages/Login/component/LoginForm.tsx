import {Form, Input, Button} from 'antd'
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import {memo} from "react";

export interface LoginFormType {
    username: string,
    password: string
}


export interface LoginFormPropsType {
    loginMethod: (form: LoginFormType) => void
}

const LoginForm = (props: LoginFormPropsType) => {
    const onFinish = (values: LoginFormType) => {
        props.loginMethod(values)
    }

    return <Form
        className="h-full overflow-hidden"
        onFinish={onFinish}
        size="large"
    >
        <div className="h-full flex flex-col items-stretch overflow-hidden">
            <div className="flex-1">
                <p className="text-center text-4xl">Login</p>
                <Form.Item
                    name="username"
                    rules={[{required: true, message: 'Please input your Username!'}]}
                >
                    <Input prefix={<UserOutlined/>} placeholder="Username"/>
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{required: true, message: 'Please input your Password!'}]}
                >
                    <Input
                        prefix={<LockOutlined/>}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
            </div>

            <Button type="primary" htmlType="submit" className="mb-10">
                Log in
            </Button>
        </div>
    </Form>

}

export default memo(LoginForm)