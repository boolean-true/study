import {Form, Input, Button} from 'antd'
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import {memo} from "react";

export interface RegisterFormType {
    username: string,
    password: string,
    again?: string
}


export interface RegisterFormPropsType {
    registerMethod: (form: RegisterFormType) => void
}

const RegisterForm = (props: RegisterFormPropsType) => {
    const [form] = Form.useForm()
    const onFinish = (values: RegisterFormType) => {
        props.registerMethod(values)
    }
    return <Form
        form={form}
        className="h-full overflow-hidden"
        onFinish={onFinish}
        size="large"
    >
        <div className="h-full flex flex-col items-stretch overflow-hidden">
            <div className="flex-1">
                <p className="text-center text-4xl">Register</p>
                <Form.Item
                    name="username"
                    rules={[{required: true, message: 'Please input your Username!'}]}
                >
                    <Input prefix={<UserOutlined/>} placeholder="Username"/>
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{required: true, message: 'Please input your Password!'}]}
                >
                    <Input
                        prefix={<LockOutlined/>}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item
                    name="again"
                    rules={[
                        {required: true, message: 'Please input your again Password!'},
                        {
                            validator: (r, v, c) => {
                                form.getFieldValue('password') === v ? c() : c('Password !== Again')
                            }
                        }
                    ]}
                >
                    <Input
                        prefix={<LockOutlined/>}
                        type="password"
                        placeholder="Again Password"
                    />
                </Form.Item>
            </div>

            <Button type="primary" htmlType="submit" className="mb-10">
                Register
            </Button>
        </div>
    </Form>

}

export default memo(RegisterForm)