import {FormEvent, useState} from "react";
import {Button, Card, Spin, message as Message} from "antd";
import styled from "styled-components";
import bg_left from '@/assets/images/login/left.svg'
import bg_right from '@/assets/images/login/right.svg'
import logo from '@/assets/images/logo/logo.svg'
import LoginForm, {LoginFormPropsType} from "./component/LoginForm";
import RegisterForm, {RegisterFormPropsType} from "./component/RegisterForm";

interface Response<T> {
    code: 200 | 400 | 500,
    data: T,
    message: string
}

interface UserInfoType {
    id: number,
    token: string,
    username: string
}

const Container = styled.div`
  &:before {
    content: "";
    width: 50%;
    background-image: url(${bg_left});
    background-repeat: no-repeat;
    background-size: cover;
  }

  &:after {
    content: "";
    width: 50%;
    background-image: url(${bg_right});
    background-repeat: no-repeat;
    background-size: cover;
  }
`

const Login = () => {
    const [isRegister, setIsRegister] = useState(false)
    const [loading, setLoading] = useState(false)
    const handleLogin: LoginFormPropsType['loginMethod'] = (values) => {
        setLoading(true)
        fetch('/api/login', {
            method: 'POST',
            body: JSON.stringify(values)
        }).then(response => response.json()).then((response: Response<UserInfoType | null>) => {
            const {code, data, message} = response
            if (code !== 200 || !data) {
                Message.error(message)
                return
            }
            const {username, id, token} = data
            Message.success(message)
        }).finally(() => setLoading(false))
    }

    const handleRegister: RegisterFormPropsType['registerMethod'] = (values) => {
        setLoading(true)
        const {username, password} = values
        const vo = {username, password}
        fetch('/api/register', {
            method: 'POST',
            body: JSON.stringify(vo)
        }).then(response => response.json()).then((response: Response<UserInfoType | null>) => {
            const {code, data, message} = response
            if (code !== 200 || !data) {
                Message.error(message)
                return
            }
            const {username, id, token} = data
            Message.success('注册成功')
        }).finally(() => setLoading(false))
    }

    return (
        <Container className="h-full w-full overflow-hidden flex justify-center items-stretch relative">
            <div className="absolute top-10 left-1/2 -translate-x-1/2  w-1/5">
                <div className="flex justify-center items-center mb-10">
                    <img className="w-1/5" src={logo} alt=""/>
                </div>
                <Spin spinning={loading}>
                    <Card>
                        <div className="text-center">
                            <div className="h-96">
                                {isRegister ? <RegisterForm registerMethod={handleRegister}/> :
                                    <LoginForm loginMethod={handleLogin}/>}
                            </div>
                            <Button type="link" onClick={() => setIsRegister(v => !v)}>没有账号？注册新账号</Button>
                        </div>
                    </Card>
                </Spin>
            </div>
        </Container>
    )
}

export default Login