type Result<T> = readonly [null, T] | readonly [Error, null]
const awaitWrapper = <T>(promise: Promise<T>): Promise<Result<T>> => {
    return promise.then(result => [null, result] as const).catch(error => [error, null] as const)
}

export default awaitWrapper