import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react'
import {resolve} from "path";

export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: [
            {find: /\/#\//, replacement: resolve(__dirname, './types')},
            {find: '@', replacement: resolve(__dirname, './src')}
        ]
    },
    server: {
        port: 2022,
        open: true
    },
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true,
                modifyVars: {
                    '@primary-color': '#0052cc'
                },
            }
        }
    },
})
