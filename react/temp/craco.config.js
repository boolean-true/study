const CracoLessPlugin = require('craco-less');
const path = require('path')
const resolve = p => path.resolve(__dirname, p)


module.exports = {
    webpack: {
        alias: {
            '@': resolve('src'),
        }
    },
    babel: {
        plugins: [
            [
                "import",
                {
                    "libraryName": "antd",
                    "libraryDirectory": "es",
                    "style": true
                }
            ]
        ]
    },
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: {'@primary-color': '#1DA57A'},
                        javascriptEnabled: true
                    }
                }
            }
        }
    ]
};