import {NavLink, Route, Switch, Redirect} from 'react-router-dom'
import Hello from './components/Hello'
import About from './pages/About'
import Home from './pages/Home'
import Other from './pages/Other'

function App() {
    return (
        <div>
            <Hello/>
            <hr/>
            <NavLink to="/home">Home</NavLink>
            <NavLink to="/about">About</NavLink>
            <NavLink to="/other">Other</NavLink>
            <Switch>
                <Route component={Home} path="/home"/>
                <Route component={About} path="/about"/>
                <Route component={Other} path="/other"/>
                <Redirect to="/home"/>
            </Switch>
        </div>
    );
}

export default App;
