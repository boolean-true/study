import React, {Component} from 'react';
import axios from "axios";

class Ajax extends Component {
    state = {
        images: []
    }

    handleSendAjax = () => {
        axios({
            method: 'get',
            url: '/cat/images/search',
            params: {limit: 1}
        }).then(response => {
            const {data} = response
            const images = data.map(v => ({id: v.id, src: v.url}))
            this.setState({images})
        }).catch(error => console.log(error))
    }

    render() {
        const {state: {images}} = this
        return (
            <div>
                <h2>Ajax</h2>
                <button onClick={this.handleSendAjax}>send ajax</button>
                <ul>
                    {images.map(v => <li key={v.id}><img src={v.src} alt=""/></li>)}
                </ul>
            </div>
        );
    }
}

export default Ajax