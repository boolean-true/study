import React, {Component} from 'react';
import store from '@/redux/store'
import {increaseAction, decreaseAction, asyncIncreaseAction} from '@/redux/actions/count'

class Count extends Component {
    componentDidMount() {
        store.subscribe(() => this.setState({}))
    }

    state = {
        num: 1
    }

    render() {
        const {state: {num}} = this
        return (
            <div>
                <h2>Redux</h2>
                <h3>和为{store.getState().count}</h3>
                <select value={num} onChange={
                    ({target}) => this.setState({num: Number.parseInt(target.value)})
                }>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                </select>
                <button onClick={() => store.dispatch(increaseAction(num))}>+</button>
                <button onClick={() => store.dispatch(decreaseAction(num))}>-</button>
                <button onClick={() => store.dispatch(asyncIncreaseAction(num))}>async increase</button>
            </div>
        );
    }
}

export default Count