import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as action from '@/redux/actions/count'

class CountOn extends Component {
    state = {
        num: 1
    }

    render() {
        const {props: {count, increaseAction, decreaseAction, asyncIncreaseAction}, state: {num}} = this
        return (
            <div>
                <h2>React Redux</h2>
                <h3>和为{count}</h3>
                <select value={num} onChange={
                    ({target}) => this.setState({num: Number.parseInt(target.value)})
                }>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                </select>
                <button onClick={() => increaseAction(num)}>+</button>
                <button onClick={() => decreaseAction(num)}>-</button>
                <button onClick={() => asyncIncreaseAction(num)}>async increase</button>
            </div>
        );
    }
}

export default connect(
    ({count}) => ({count}),
    action
)(CountOn);