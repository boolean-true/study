import React, {Component} from 'react';

class Search extends Component {
    state = {
        search: ''
    }

    handleSubmitSearchForm = (e) => {
        e.preventDefault()
        e.stopPropagation()
        const {state: {search}} = this
        this.props.handleSearch(search)
    }

    render() {
        const {handleSubmitSearchForm} = this
        return (
            <form action="" onSubmit={handleSubmitSearchForm}>
                <input type="text" name="search" autoComplete="off"
                       onChange={e => this.setState({search: e.target.value})}/>
                <button type="submit">search</button>
            </form>
        );
    }
}

export default Search