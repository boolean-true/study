import React, {Component} from 'react';
import Search from "./Search";
import axios from "axios";

class Github extends Component {
    state = {
        user: []
    }
    handleSearch = (keyword) => {
        axios({
            url: '/github/search/users',
            method: 'get',
            params: {q: keyword}
        }).then(({data}) => {
            const {items} = data
            this.setState({user: items})
        }).catch(e => console.log(e))
    }

    render() {
        const {handleSearch, state: {user}} = this
        return (
            <div>
                <h2>Github</h2>
                <Search handleSearch={handleSearch}/>
                <ul>
                    {
                        user.map(v => <li key={v.id}>
                            <img src={v.avatar_url} alt=""/>
                        </li>)
                    }
                </ul>
            </div>
        );
    }
}

export default Github