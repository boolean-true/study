import {withRouter} from 'react-router-dom'
import styles from './index.module.css'

const Hello = (props) => {
    return (
        <div className={styles.scope}>
            <h1>hello React</h1>
            <button onClick={props.history.goBack}>回退</button>
            <button onClick={props.history.goForward}>前进</button>
        </div>
    )
}

export default withRouter(Hello)