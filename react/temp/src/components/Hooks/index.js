import {useEffect, useMemo, useState, Fragment} from "react";


const A = (<h1>A</h1>)
const B = (<h1>B</h1>)


export default () => {
    const [Child, setChild] = useState('A')
    return (
        <Fragment>
            <button onClick={() => setChild('B')}>button</button>
            {/**/}
            {/*<A></A>*/}
            {<Child/>}
        </Fragment>
    )
}