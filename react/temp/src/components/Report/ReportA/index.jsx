import React, {Component} from 'react';
import PubSub from 'pubsub-js'

class ReportA extends Component {
    sendMessage = () => {
        PubSub.publish('formA', 'hello ReportB, I am ReportA')
    }

    render() {
        return (
            <div>
                <h3>ReportA</h3>
                <button onClick={this.sendMessage}>send message to B</button>
            </div>
        );
    }
}

export default ReportA