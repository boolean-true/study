import React, {Component} from 'react';
import PubSub from 'pubsub-js'

class ReportB extends Component {
    state = {
        message: ''
    }

    componentDidMount() {
        PubSub.subscribe('formA', (title, payload) => {
            this.setState({message: payload})
        })
    }

    componentWillUnmount() {
        PubSub.unsubscribe('formA')
    }

    render() {
        const {state: {message}} = this
        const messageNode = message ? (
            <dl>
                <dt>收到消息了：</dt>
                <dd>{message}</dd>
            </dl>
        ) : null
        return (
            <div>
                <h3>ReportB</h3>
                {messageNode}
            </div>
        );
    }
}

export default ReportB