import React, {Component} from 'react';
import ReportA from "./ReportA";
import ReportB from "./ReportB";
class Report extends Component {
    render() {
        return (
            <div>
                <h2>兄弟组件通讯 发布订阅</h2>
                <ReportA/>
                <ReportB/>
            </div>
        );
    }
}

export default Report