import React, {Component} from 'react';

export default class Introducer extends Component {
    handleKeyUp = (e) => {
        const {target: {value}, keyCode} = e
        if (keyCode !== 13 || !value) return
        this.props.handleCreateItem({id: Date.now(), content: value, done: false})
        e.target.value = ''
    }

    render() {
        return <input type="text" onKeyUp={this.handleKeyUp}/>
    }
}