import React, {Component} from 'react';

export default class Item extends Component {
    handleCheckboxChange = (id, e) => {
        const {target: {checked}} = e
        this.props.handleUpdateItem(id, 'done', checked)
    }

    render() {
        const {props: {id, content, done, handleRemoveItem}} = this
        return (
            <li>
                <input type="checkbox" checked={done} onChange={e => this.handleCheckboxChange(id, e)}/>
                <span>{content}</span>
                <button onClick={() => handleRemoveItem(id)}>删除</button>
            </li>
        )
    }

    static defaultProps = {
        id: 0,
        content: '',
        done: false
    }
}