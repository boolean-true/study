import React, {Component} from 'react';

export default class Wrapper extends Component {
    handleAllCheckboxChange = (e) => {
        const {target: {checked}} = e
        this.props.handleDoneAll(checked)
    }

    render() {
        const {props: {done, total, checked, children}, handleAllCheckboxChange} = this
        return (
            <div>
                <ul>{children}</ul>
                <div>
                    <input checked={checked} type="checkbox" onChange={handleAllCheckboxChange}/>
                    <span>{done} / {total}</span>
                    <button onClick={this.props.handleRemoveDone}>清空已完成</button>
                </div>
            </div>
        );
    }
}