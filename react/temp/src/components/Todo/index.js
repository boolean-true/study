import React, {Component} from 'react';
import Introducer from "./components/Introducer";
import Wrapper from "./components/Wrapper";
import Item from "./components/Item";

export default class Todo extends Component {
    state = {
        todo: [
            {id: 1, content: '吃饭', done: false},
            {id: 2, content: '睡觉', done: true}
        ]
    }

    handleCreateItem = (item) => {
        const {todo} = this.state
        this.setState({todo: [item, ...todo]})
    }

    handleUpdateItem = (id, label, value) => {
        const {todo} = this.state
        const index = todo.findIndex(v => v.id === id)
        todo[index][label] = value
        this.setState({todo})
    }

    handleRemoveItem = (id) => {
        const {todo} = this.state
        const index = todo.findIndex(v => v.id === id)
        todo.splice(index, 1)
        this.setState({todo})
    }

    handleRemoveDone = () => {
        const {todo} = this.state
        const done = todo.filter(v => !v.done)
        this.setState({todo: done})
    }

    handleDoneAll = (done) => {
        const {todo} = this.state
        todo.forEach(v => v.done = done)
        this.setState({todo})
    }

    render() {
        const {
            state: {todo},
            handleCreateItem,
            handleUpdateItem,
            handleRemoveItem,
            handleRemoveDone,
            handleDoneAll
        } = this
        const doneCount = todo.reduce((p, c) => c.done ? p + 1 : p, 0)
        return (
            <div>
                <h2>TODO</h2>
                <Introducer handleCreateItem={handleCreateItem}/>
                <Wrapper total={todo.length}
                         done={doneCount}
                         checked={doneCount === todo.length}
                         handleRemoveDone={handleRemoveDone}
                         handleDoneAll={handleDoneAll}
                >
                    {
                        todo.map(v => <Item key={v.id} {...v}
                                            handleUpdateItem={handleUpdateItem}
                                            handleRemoveItem={handleRemoveItem}
                        />)
                    }
                </Wrapper>
            </div>
        )
    }
}