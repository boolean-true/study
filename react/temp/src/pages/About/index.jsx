import React, {Component} from 'react';
import {Provider} from 'react-redux'

import store from "@/redux/store";
import Todo from '@/components/Todo'
import Ajax from '@/components/Ajax'
import Github from '@/components/Github'
import Report from '@/components/Report'
import Count from '@/components/Count'
import CountOn from '@/components/CountOn'

class About extends Component {
    routerGo = () => {
        this.props.history.push('/home/home_a/3')
    }

    render() {
        return (
            <div>
                <h1>About</h1>
                <button onClick={this.routerGo}>to Home A route 3</button>
                <Todo/>
                <hr/>
                <Ajax/>
                <hr/>
                <Github/>
                <hr/>
                <Report/>
                <hr/>
                <Count/>
                <hr/>
                <Provider store={store}>
                    <CountOn/>
                </Provider>
            </div>
        );
    }
}

export default About