import React, {Component} from 'react';

class Detail extends Component {
    render() {
        const {props: {match: {params}}} = this
        return (
            <div>
                <h4>HomeA Detail</h4>
                <p>接收到参数为{params.id}</p>
            </div>
        );
    }
}

export default Detail