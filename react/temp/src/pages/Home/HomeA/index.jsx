import React, {Component} from 'react';
import {NavLink, Route, Redirect, Switch} from 'react-router-dom'
import Detail from "./Detail";

class HomeA extends Component {
    state = {
        navMap: [
            {id: 1, title: 'home A route 1'},
            {id: 2, title: 'home A route 2'},
            {id: 3, title: 'home A route 3'}
        ]
    }

    render() {
        const {state: {navMap}} = this
        const [first] = navMap
        const {id: defaultId} = first
        return (
            <div>
                <h2>HomeA</h2>
                <h3>params 传参</h3>
                {
                    navMap.map(v => <NavLink to={`/home/home_a/${v.id}`} key={v.id}>{v.title}</NavLink>)
                }
                <Switch>
                    <Route path="/home/home_a/:id" component={Detail}/>
                    <Redirect to={`/home/home_a/${defaultId}`}/>
                </Switch>
            </div>
        );
    }
}

export default HomeA