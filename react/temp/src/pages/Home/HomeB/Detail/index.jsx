import React, {Component} from 'react';
import qs from 'querystring'

class Detail extends Component {
    render() {
        const {props: {location: {search}}} = this
        const query = qs.parse(search.slice(1))
        return (
            <div>
                <h4>HomeA Detail</h4>
                <p>接收到参数为{query.id}</p>
            </div>
        );
    }
}

export default Detail