import React, {Component} from 'react';
import {Link, Redirect, Route, Switch} from "react-router-dom";
import Detail from "./Detail";

class HomeB extends Component {
    state = {
        navMap: [
            {id: 1, title: 'home B route 1'},
            {id: 2, title: 'home B route 2'},
            {id: 3, title: 'home B route 3'}
        ]
    }

    render() {
        const {state: {navMap}} = this
        const [first] = navMap
        const {id: defaultId} = first
        return (
            <div>
                <h2>HomeB</h2>
                <h3>search 传参</h3>
                {
                    navMap.map(v => <Link to={`/home/home_b/detail?id=${v.id}`} key={v.id}>{v.title}</Link>)
                }
                <Switch>
                    <Route path="/home/home_b/detail" component={Detail}/>
                    <Redirect to={`/home/home_b/detail?id=${defaultId}`}/>
                </Switch>
            </div>
        )
    }
}

export default HomeB