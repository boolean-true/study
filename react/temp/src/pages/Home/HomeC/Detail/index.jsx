import React, {Component} from 'react';

class Detail extends Component {
    render() {
        const {props: {location: {state}}} = this
        console.log(state)
        return (
            <div>
                <h4>HomeA Detail</h4>
                <p>接收到参数为{JSON.stringify(state)}</p>
            </div>
        );
    }
}

export default Detail