import React, {Component} from 'react';
import {Link, Route, Switch} from "react-router-dom";
import Detail from "./Detail";

class HomeC extends Component {
    state = {
        navMap: [
            {id: 1, title: 'home C route 1'},
            {id: 2, title: 'home C route 2'},
            {id: 3, title: 'home C route 3'}
        ]
    }

    render() {
        const {state: {navMap}} = this
        return (
            <div>
                <h2>HomeC</h2>
                <h3>state 传参</h3>
                {
                    navMap.map(v => <Link to={{pathname: '/home/home_c/detail', state: {...v}}}
                                          key={v.id}>{v.title}</Link>)
                }
                <Switch>
                    <Route path="/home/home_c/detail" component={Detail}/>
                </Switch>
            </div>
        );
    }
}

export default HomeC