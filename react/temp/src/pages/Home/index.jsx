import React, {Component} from 'react';
import {NavLink, Switch, Route, Redirect} from 'react-router-dom'
import HomeA from "./HomeA";
import HomeB from "./HomeB";
import HomeC from "./HomeC";

class Home extends Component {
    render() {
        return (
            <div>
                <h1>Home</h1>
                <NavLink to="/home/home_a">HomeA</NavLink>
                <NavLink to="/home/home_b">HomeB</NavLink>
                <NavLink to="/home/home_c">HomeC</NavLink>
                <Switch>
                    <Route path="/home/home_a" component={HomeA}/>
                    <Route path="/home/home_b" component={HomeB}/>
                    <Route path="/home/home_c" component={HomeC}/>
                    <Redirect to="/home/home_a"/>
                </Switch>
            </div>
        );
    }
}

export default Home