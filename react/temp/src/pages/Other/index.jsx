import React, {Component, useRef} from 'react';
import {Button, Form, Input, Tooltip} from 'antd';
import {SearchOutlined} from '@ant-design/icons';

import Hooks from '@/components/Hooks'

const Other = () => {
    const inputRef = useRef()
    const formRef = useRef()
    return (
        <>
            <Input ref={inputRef}/>
            <Form ref={formRef}/>
            <Button onClick={() => console.log(formRef)}>button</Button>
        </>
    )
}

export default Other
