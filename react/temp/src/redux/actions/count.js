import {INCREASE, DECREASE} from '../constant'

export const increaseAction = data => ({type: INCREASE, data})
export const decreaseAction = data => ({type: DECREASE, data})
export const asyncIncreaseAction = data => dispatch => {
    setTimeout(() => dispatch(increaseAction(data)), 500)
}