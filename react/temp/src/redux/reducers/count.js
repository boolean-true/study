import {INCREASE, DECREASE} from '../constant'

const repeaters = {
    [INCREASE]: (pre, cur) => pre + cur,
    [DECREASE]: (pre, cur) => pre - cur
}

const countReducer = (preState = 0, {type = '', data = 0}) => {
    const handle = repeaters[type]
    if (handle) return handle(preState, data)
    else return preState
}

export default countReducer