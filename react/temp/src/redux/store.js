import {createStore, applyMiddleware, combineReducers} from 'redux'
import reduxThunk from 'redux-thunk'

import countReducer from "./reducers/count";

export default createStore(combineReducers({
    count: countReducer
}), applyMiddleware(reduxThunk))