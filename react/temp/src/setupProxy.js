const {createProxyMiddleware} = require('http-proxy-middleware')

module.exports = (app) => {
    app.use(createProxyMiddleware('/cat', {
        target: 'https://api.thecatapi.com',
        changeOrigin: true,
        secure: false,
        pathRewrite: {
            '/cat': '/v1'
        }
    }))

    app.use(createProxyMiddleware('/github', {
        target: 'https://api.github.com',
        changeOrigin: true,
        secure: false,
        pathRewrite: {
            '/github': ''
        }
    }))
}