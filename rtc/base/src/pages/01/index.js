(async () => {
    const audio = document.querySelector('audio')
    const stop = document.querySelector('#stop')
    const start = document.querySelector('#start')
    /* 获取所有设备 */
    // const devices = await navigator.mediaDevices.enumerateDevices()
    const stream = await navigator.mediaDevices.getUserMedia({audio: true})
    const recorder = new window.MediaRecorder(stream)

    recorder.ondataavailable = (e) => {
        console.log(stream)
        audio.src = URL.createObjectURL(e.data)
    }

    start.addEventListener('click', () => {
        console.log('开始录制')
        recorder.start()
    })

    stop.addEventListener('click', () => {
        console.log('停止录制')
        recorder.stop()
        stream.getTracks()[0].stop()
    })
})()
