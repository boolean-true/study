import * as tf from '@tensorflow/tfjs'
import * as tfvis from '@tensorflow/tfjs-vis'

/* 线性回归 */

const xs = [1, 2, 3, 4]
const ys = [2, 3, 4, 5]

 tfvis.render.scatterplot(
    {drawArea: document.querySelector('#container')},
    {values: xs.map((x, i) => ({x, y: ys[i]}))},
    {xAxisDomain: [0, 6], yAxisDomain: [0, 8]}
)


// 初始模型 （连续模型 下一层的输入是上一层的输出）
const model = tf.sequential()
// 添加层（全连接层 输出层 ） 1个神经元（神经网络由多个神经元构成）
model.add(tf.layers.dense({units: 1, inputShape: [1]}))
// loss 损失函数（均方误差）
// optimizer 优化器 梯度下降
model.compile({loss: tf.losses.meanSquaredError, optimizer: tf.train.sgd(.1)})

const inputs = tf.tensor(xs)
const labels = tf.tensor(ys)
// 拟合
model.fit(inputs, labels, {
    batchSize: 4,
    epochs: 60,
    callbacks: tfvis.show.fitCallbacks({drawArea: document.querySelector('#container')}, ['loss'])
}).then(() => {
    const output = model.predict(tf.tensor([20])) // 预测
    console.log(output.dataSync())
})
