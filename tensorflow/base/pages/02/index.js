import * as tf from '@tensorflow/tfjs'
import * as tfvis from '@tensorflow/tfjs-vis'

// 归一化
const container = document.getElementById('container')


const heights = [150, 160, 170]
const weights = [40, 50, 60]

tfvis.render.scatterplot(
    {drawArea: container},
    {
        values: heights.map((v, i) => ({x: v, y: weights[i]}))
    },
    {
        xAxisDomain: [140, 180],
        yAxisDomain: [30, 70]
    }
)

const inputs = tf.tensor(heights).sub(150).div(20) // 压缩(减去最小值 除以 宽度（最大值减去最小值）)
const labels = tf.tensor(weights).sub(40).div(20)

const model = tf.sequential()
model.add(tf.layers.dense({units: 1, inputShape: [1]}))
model.compile({loss: tf.losses.meanSquaredError, optimizer: tf.train.sgd(.1)})
model.fit(inputs, labels, {
    batchSize: 3,
    epochs: 100,
    callbacks: tfvis.show.fitCallbacks({drawArea: container}, ['loss'])
}).then(() => {
    const temp = tf.tensor([180]).sub(150).div(20) // 入参归一化
    const output = model.predict(temp).mul(20).add(40) // 结果反归一化
    console.log(output.dataSync())
})


