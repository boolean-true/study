import * as tf from '@tensorflow/tfjs'
import * as tfvis from '@tensorflow/tfjs-vis'

import {getData} from './data'


// 逻辑回归
const container = document.getElementById('container')
const form = document.querySelector('form')

let completed = false

const data = getData(400)
tfvis.render.scatterplot(
    {drawArea: container},
    {
        values: [
            data.filter(v => v.label === 1),
            data.filter(v => v.label === 0)
        ]
    }
)


const model = tf.sequential()
model.add(tf.layers.dense({
    units: 1,
    inputShape: [2],
    activation: 'sigmoid'
}))

model.compile({loss: tf.losses.logLoss, optimizer: tf.train.adam(.1)})

const inputs = tf.tensor(data.map(v => [v.x, v.y]))
const labels = tf.tensor(data.map(v => v.label))
model.fit(inputs, labels, {
    batchSize: 40,
    epochs: 50,
    callbacks: tfvis.show.fitCallbacks(
        {drawArea: container},
        ['loss', 'acc'] // 度量单位
    )
}).then(() => {
    completed = true
})

form.addEventListener('submit', e => {
    e.stopPropagation()
    e.preventDefault()
    if (!completed) return console.log('还没训练完')
    const formElement = e.target
    const inputElements = formElement.querySelectorAll('input[name]')
    const data = {}
    Array.from(inputElements).forEach(v => {
        data[v.name] = Number.parseFloat(v.value) || 0
    })

    const inputs = tf.tensor([[data.x, data.y]])
    const output = model.predict(inputs)
    console.log(`[${data.x},${data.y}]的预测为${output.dataSync()[0]}`)
})
