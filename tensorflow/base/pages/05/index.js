import * as tf from '@tensorflow/tfjs'
import * as tfvis from '@tensorflow/tfjs-vis'

import {getIrisData, IRIS_CLASSES} from './data'
const form = document.querySelector('form')
const container = document.getElementById('container')
let completed = false
// 多层神经网络
const [xTrain, yTrain, xTest, yTest] = getIrisData(.15)
const model = tf.sequential()
model.add(tf.layers.dense({units: 10, inputShape: [xTrain.shape[1]], activation: 'sigmoid'}))
model.add(tf.layers.dense({units: 3, activation: 'softmax'}))
// 交叉熵损失函数 metrics度量
model.compile({loss: 'categoricalCrossentropy', optimizer: tf.train.adam(.1), metrics: ['accuracy']})

model.fit(xTrain, yTrain, {
    epochs: 50,
    validationData: [xTest, yTest],
    callbacks: tfvis.show.fitCallbacks(
        {drawArea: container},
        ['loss', 'acc', 'val_loss', 'val_acc']
    )
}).then(() => {
    completed = true
})


form.addEventListener('submit', e => {
    e.stopPropagation()
    e.preventDefault()
    if (!completed) return console.log('还没训练完')
    const formElement = e.target
    const inputElements = formElement.querySelectorAll('input[name]')
    const data = {}
    Array.from(inputElements).forEach(v => {
        data[v.name] = Number.parseFloat(v.value) || 0
    })

    const inputs = tf.tensor([[data.a, data.b, data.c, data.d]])
    const output = model.predict(inputs)
    console.log(`[${data.a},${data.b},${data.c},${data.d}]的预测为${IRIS_CLASSES[output.argMax(1).dataSync()[0]]}`)
})
