import * as tf from '@tensorflow/tfjs'
import * as tfvis from '@tensorflow/tfjs-vis'

// import {getData} from '../04/data' // 逻辑回归 复杂
import {getData} from './data' // 线性回归 简单

// 欠拟合和过拟合
const container = document.getElementById('container')
const data = getData(200, 3)

tfvis.render.scatterplot(
    {drawArea: container},
    {
        values: [
            data.filter(v => v.label === 1),
            data.filter(v => v.label === 0)
        ]
    }
)


const model = tf.sequential()
/*
model.add(tf.layers.dense({units: 1, inputShape: [2], activation: 'sigmoid'})) // 欠拟合
* */
model.add(tf.layers.dense({
    units: 10,
    inputShape: [2],
    activation: "tanh",
    kernelRegularizer: tf.regularizers.l2({l2: 1}) // 正则化 权限衰减
})) // 隐藏层
// model.add(tf.layers.dropout({rate: .9})) // 丢弃层
model.add(tf.layers.dense({units: 1, activation: 'sigmoid'})) // 输出层

const inputs = tf.tensor(data.map(v => [v.x, v.y]))
const labels = tf.tensor(data.map(v => v.label))

model.compile({
    loss: tf.losses.logLoss,
    optimizer: tf.train.adam(.1)
})

model.fit(inputs, labels, {
    epochs: 200,
    validationSplit: .2,
    callbacks: tfvis.show.fitCallbacks(
        {drawArea: container},
        ['loss', 'val_loss']
    )
})
