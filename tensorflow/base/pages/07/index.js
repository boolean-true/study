import * as tf from '@tensorflow/tfjs'
import * as tfvis from '@tensorflow/tfjs-vis'

import {MnistData} from './data'

const data = new MnistData()
data.load().then(() => {
    const examples = data.nextTestBatch(20)
    console.log(examples)
})
