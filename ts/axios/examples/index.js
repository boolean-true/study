const express = require('express')
const bodyParser = require('body-parser')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const WebpackConfig = require('./webpack.config')

const routes = require('./routes')

const app = express()
const compiler = webpack(WebpackConfig)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(webpackDevMiddleware(compiler, {
  publicPath: '/__build__/',
  stats: {
    colors: true,
    chunks: false
  }
}))

app.use(webpackHotMiddleware(compiler))

app.use(express.static(__dirname))


app.use('/api', (request, response, next) => {
  console.log('收到请求了：', request.path)
  next()
}, routes)

const server = app.listen(process.env.PORT || 8080, 'localhost', () => {
  console.log('Example app listening at http://%s:%s', server.address().address, server.address().port)
})

module.exports = server
