/*
* @data为返回的数据
* @message为返会的提示信息
* */
class BaseModel {
  constructor(data, message) {
    if (typeof data === 'string') {
      this.message = data
      message = null
      data = null
    }

    if (data) {
      this.data = data
    }

    if (message) {
      this.message = message
    }
  }
}

class Success extends BaseModel {
  constructor(data, message) {
    super(data, message)
    this.code = 0
    this.message = 'success'
  }
}

class Error extends BaseModel {
  constructor(data, message) {
    super(data, message)
    this.code = -1
    this.message = 'fail'
  }
}

module.exports = {
  Success,
  Error
}
