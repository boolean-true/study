const {Router} = require('express')
const router = Router()

const {Success} = require('../model/response')

router.get('/get', (request, response) => {
  const {query} = request
  response.json(new Success(query))
})

router.post('/post', (request, response) => {
  const {body} = request
  response.json(new Success(body))
})

module.exports = router
