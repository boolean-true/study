import { isDate, isObject } from '../utils/typeCheck'

const encode = (v: string): string =>
  encodeURIComponent(v)
    .replace(/%3A/gi, ':')
    .replace(/%24/g, '$')
    .replace(/%2C/gi, ',')
    .replace(/%20/g, '+')
    .replace(/%5B/gi, '[')
    .replace(/%5D/gi, ']')

export default (url: string, params?: object): string => {
  if (!params) return url
  const parts: string[] = []

  for (let [k, v] of Object.entries(params)) {
    if (v === null || typeof v === 'undefined') continue
    if (Array.isArray(v)) k += '[]'
    else v = [v]

    v.forEach((o: any) => {
      if (isDate(o)) o = o.toISOString()
      else if (isObject(o)) o = JSON.stringify(o)
      parts.push(encode(k) + '=' + encode(o))
    })
  }

  const serializedParams = parts.join('&')
  if (!serializedParams) return url
  const hashMarkIndex = url.indexOf('#')
  if (hashMarkIndex > -1) url = url.slice(0, hashMarkIndex)
  url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams
  return url
}
