import { isObject } from '../utils/typeCheck'

const transformRequest = (data: any): any => {
  if (isObject(data)) return JSON.stringify(data)
  return data
}

export { transformRequest }
// https://www.python.org/ftp/python/3.9.7/Python-3.9.7.tar.xz
