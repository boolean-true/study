import { isObject } from '../utils/typeCheck'

const formattingHeaders = (headers: any, name: string): any => {
  if (!headers) return headers
  const upName = name.toUpperCase()
  for (let [k, v] of Object.entries(headers)) {
    if (k !== name && k.toUpperCase() === upName) {
      headers[name] = v
      delete headers[k]
    }
  }
  return headers
}

const transformHeaders = (headers: any, data: any): any => {
  if (!headers) return headers
  headers = formattingHeaders(headers, 'Content-Type')

  if (isObject(data)) {
    if (!headers['Content-Type']) {
      headers['Content-Type'] = 'application/json;charset=utf-8'
    }
  }

  return headers
}

export { transformHeaders }
