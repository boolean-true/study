// types
import { AxiosConfig } from './types'

// core
import xhr from './xhr'

// utils
import buildUrl from './helpers/buildUrl'
import { transformRequest } from './helpers/data'
import { transformHeaders } from './helpers/headers'

const processConfig = (config: AxiosConfig): void => {
  const { url, params, data, headers = {} } = config
  config.url = buildUrl(url, params)
  config.data = transformRequest(data)
  config.headers = transformHeaders(headers, data)
}

export default (config: AxiosConfig): void => {
  processConfig(config)
  xhr(config)
}
