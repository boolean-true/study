export type Method = 'GET' | 'POST'

export interface AxiosConfig {
  url: string
  method?: Method
  data?: any
  params?: any
  headers?: any
}
