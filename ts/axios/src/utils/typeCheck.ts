const { toString } = Object.prototype

export const isObject = (v: any): v is Object => toString.call(v) === '[object Object]'

export const isDate = (v: any): v is Date => toString.call(v) === '[object Date]'
