import { AxiosConfig } from './types'

export default (config: AxiosConfig): void => {
  const { url, method = 'GET', data = null, headers = {} } = config
  const request = new XMLHttpRequest()
  request.open(method.toUpperCase(), url, true)
  for (const [k, v] of Object.entries(headers)) {
    if (data === null && k.toLowerCase() === 'content-type') {
      delete headers[k]
    } else {
      request.setRequestHeader(k, typeof v === 'string' ? v : '')
    }
  }
  request.send(data)
}
