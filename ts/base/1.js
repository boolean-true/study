function demo(data) {
    return data.x + data.y;
}
var result = demo({ x: 6, y: 2 });
console.log(result);
var person = {
    name: 'cxk',
    age: 18
};
var arr = [1, 2, 4];
var Base = /** @class */ (function () {
    function Base() {
    }
    return Base;
}());
var base = new Base();
var getNum = function () {
    return 2 + 2;
};
console.log(getNum());
var con = 1;
con = '100';
var one = [1, '2', 3, false];
var two = [{ name: 'cxk', age: 18 }, { name: 'demo', age: 1 }];
// 元组
var three = ['ok', 'no', 1];
var four = [
    ['ok', 'on', 1],
    ['ok2', 'on2', 2],
    ['ok3', 'on3', 3],
];
var five = {
    name: 'cxk',
    gander: 'male',
    size: 100,
    say: function (str) {
        if (str === void 0) { str = ''; }
        return str;
    }
};
// 应用接口
var Six = /** @class */ (function () {
    function Six() {
        this.name = 'ok';
        this.gander = 'male';
    }
    Six.prototype.say = function (str) {
        return str;
    };
    return Six;
}());
var seven = new Six();
console.log(seven);
var Eight = /** @class */ (function () {
    function Eight() {
    }
    Eight.prototype.say = function (str) {
        return str;
    };
    ;
    Eight.prototype.sing = function () {
        return 'sing';
    };
    return Eight;
}());
var nine = function (str) {
    return str;
};
