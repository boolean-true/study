interface demoData {
  x: number,
  y: number
}

function demo(data: demoData) {
  return data.x + data.y
}

const result = demo({x: 6, y: 2});

console.log(result);

const person: {
  name: string,
  age: number
} = {
  name: 'cxk',
  age: 18
}

const arr: number[] = [1, 2, 4];

class Base {
}

const base: Base = new Base();

const getNum: () => number = () => {
  return 2 + 2;
}


console.log(getNum());

let con: number | string = 1;
con = '100';

const one: (string | number | boolean)[] = [1, '2', 3, false];

const two: { name: string, age: number }[] = [{name: 'cxk', age: 18}, {name: 'demo', age: 1}];

type threeType = [string, string, number];
// 元组
const three: threeType = ['ok', 'no', 1];

const four: (threeType)[] = [
  ['ok', 'on', 1],
  ['ok2', 'on2', 2],
  ['ok3', 'on3', 3],
]

interface fiveType {
  name: string,
  age?: number, // 表示可有可无
  readonly gander: string, // readonly 表示只读 不可修改
  [propName: string]: any, // 可能会有其他未知值
  say(string): string
}

const five: fiveType = {
  name: 'cxk',
  gander: 'male',
  size: 100,
  say(str = '') {
    return str;
  }
}

// 应用接口
class Six implements fiveType {
  name = 'ok';
  gander = 'male';

  say(str): string {
    return str;
  }
}

const seven: fiveType = new Six();

console.log(seven);

/* ----------- */

interface eightType extends fiveType {
  sing(): string; // 接口的继承
}

class Eight implements eightType {
  name: 'Tom';
  age: 18;
  gander: 'male';

  say(str) {
    return str;
  };

  sing() {
    return 'sing';
  }
}

interface nineType {
  (str: string): string;
}

const nine: nineType = (str: string) => {
  return str;
}