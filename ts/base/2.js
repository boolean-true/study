var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person() {
        this.name = "None"; // public 类内，内外，子类中都可使用
        this.age = 18; // protected 只能在类内和子类中使用
        this.gender = "male"; // private 只能在内类使用
    }
    return Person;
}());
var Tom = /** @class */ (function (_super) {
    __extends(Tom, _super);
    function Tom() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Tom.prototype.TomLogger = function () {
        console.log(this.name);
        console.log(this.age);
        // console.log(p.gender) // 不可使用
    };
    return Tom;
}(Person));
var p = new Person();
// console.log(p.name)
/*
console.log(p.age) // 不可使用
console.log(p.gender) // 不可使用
*/
/************/
var Base = /** @class */ (function () {
    function Base(_description) {
        this._description = _description;
    }
    Object.defineProperty(Base.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (description) {
            this._description = description.toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    return Base;
}());
var base = new Base("cookie");
console.log(base.description);
base.description = "session";
console.log(base.description);
