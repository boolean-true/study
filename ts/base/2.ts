class Person {
  public name = "None"; // public 类内，内外，子类中都可使用
  protected age = 18; // protected 只能在类内和子类中使用
  private gender = "male" // private 只能在内类使用
}

class Tom extends Person {
  TomLogger() {
    console.log(this.name)
    console.log(this.age)
    // console.log(p.gender) // 不可使用
  }
}


const p = new Person();

// console.log(p.name)
/*
console.log(p.age) // 不可使用
console.log(p.gender) // 不可使用
*/

/************/


class Base {
  constructor(private _description: string) {

  }

  get description() {
    return this._description;
  }

  set description(description: string) {
    this._description = description.toUpperCase();
  }
}

const base = new Base("cookie");

console.log(base.description);

base.description = "session";

console.log(base.description);
