/* 单例 */
var Demo = /** @class */ (function () {
    /*定义一个私有的constructor*/
    function Demo() {
    }
    ;
    Demo.initDemo = function () {
        if (!this.instance) {
            this.instance = new Demo();
        }
        return this.instance;
    };
    return Demo;
}());
console.log(Demo.initDemo());
