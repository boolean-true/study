/* 单例 */
class Demo {
  private static instance: Demo;

  /*定义一个私有的constructor*/
  private constructor() {

  };

  static initDemo() {
    if (!this.instance) {
      this.instance = new Demo();
    }
    return this.instance;
  }
}

console.log(Demo.initDemo())