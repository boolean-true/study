var Demo = /** @class */ (function () {
    function Demo(_description) {
        this._description = _description;
    }
    Object.defineProperty(Demo.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (description) {
            this._description = description;
        },
        enumerable: false,
        configurable: true
    });
    return Demo;
}());
var demo = new Demo("cookie");
console.log(demo.description);
