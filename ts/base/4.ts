/*
class Demo {
  constructor(private _description: string) {
  }

  get description() {
    return this._description
  }

  set description(description: string) {
    this._description = description
  }
}

const demo = new Demo("cookie");

console.log(demo.description);
*/

/* 抽象类 */
abstract class Geom {
  width: number;
  height: number;
  abstract getArea(): number;
}

class Square extends Geom {
  getArea() {
    return 100;
  }
}

class Rect extends Geom{
  getArea() {
    return 200;
  }
}

const s = new Square()
console.log(s.getArea())