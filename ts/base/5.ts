// 枚举

enum Direction {
  up,
  down,
  left,
  right
}

console.log(Direction.down);

// 常量枚举 只有常量值才可以常量枚举

const enum Color {
  red = "red",
  cyan = "cyan"
}

if (Color.cyan === "cyan") {
  console.log("color is cyan")
}