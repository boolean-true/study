// 泛型
function echo(arg) {
    return arg;
}
var echoResult = echo(100);
console.log(echoResult);
function reverse(arr) {
    return [arr[1], arr[0]];
}
var reverseResult = reverse(["string", 1000]);
console.log(reverseResult);
function echoWithLength(arg) {
    console.log(arg.length);
    return arg;
}
var lengthOfString = echoWithLength("string");
var lengthOfArray = echoWithLength([1, 2, 3]);
