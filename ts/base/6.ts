// 泛型

function echo<T>(arg: T): T {
  return arg
}

const echoResult = echo(100);
console.log(echoResult);

function reverse<T, U>(arr: [T, U]): [U, T] {
  return [arr[1], arr[0]]
}

const reverseResult = reverse(["string", 1000]);
console.log(reverseResult);

// 约束泛型
interface IWithLength {
  length: number
}

function echoWithLength<T extends IWithLength>(arg: T): T {
  console.log(arg.length)
  return arg;
}

const lengthOfString = echoWithLength("string");
const lengthOfArray = echoWithLength([1, 2, 3]);
const lengthOfObject = echoWithLength({length: 100});

// 类与泛型

class Queue<T> {
  private data = [];

  push(item: T) {
    this.data.push(item);
  }

  pop(): T {
    return this.data.shift();
  }
}

const queue = new Queue<number>();
queue.push(0);
console.log(queue.pop().toFixed(2));


// 泛型与接口

interface KeyPair<T, U> {
  label: U,
  value: T
}

const option: KeyPair<number, string> = {
  value: 0,
  label: "success"
}

//

const myArray: Array<number> = [1, 3, 4];
