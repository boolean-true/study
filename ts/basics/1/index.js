// boolean
var flag = false;
// number
var num = 12;
// bigint
// const big: bigint = 100n // > ES2020
// string
var str = 'hello';
// Array<number>
var numberArr = [1, 2];
// 元组
var tuple = [1, '1'];
// 枚举
var Gender;
(function (Gender) {
    Gender[Gender["male"] = 0] = "male";
    Gender[Gender["female"] = 1] = "female";
})(Gender || (Gender = {}));
var mine = {
    name: '彭于晏',
    gender: Gender.male
};
// any
var temp = 1;
// void
function foo() {
    console.log('wo ~ ing');
}
// undefined
var un = undefined;
// null
var nu = null;
// never
function error(message) {
    throw new Error(message);
}
function loop() {
    // 永远不结束
    while (true) {
    }
}
// function
var sum = function (x, y) {
    return x + y;
};
// 联合类型
var dom = 1;
// 交叉类型
var you = {
    age: 19,
    gender: Gender.female
};
// 类型断言
var unknown = 1;
// const unKnownLength: number = (<string>unknown).length
var unKnownLength = unknown.length;
var nowTime = new Date();
var cxk = {
    name: 'cxk',
    age: 12,
    hobby: ['唱', '跳', 'rap', '篮球']
};
function sayHello(person) {
    var name = person.name, age = person.age, hobby = person.hobby;
    console.log("hello, my name is " + name + ", age is " + age + ", I like " + hobby.join(','));
}
sayHello(cxk);
// 只读
var onlyArr = [1, 2, 3];
var sayGoodBye = function (name) {
    console.log("Good-bye " + name);
};
