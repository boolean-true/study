// boolean
const flag: boolean = false

// number
const num: number = 12

// bigint
// const big: bigint = 100n // > ES2020

// string
const str: string = 'hello'

// Array<number>
const numberArr: number[] = [1, 2]

// 元组
const tuple: [number, string] = [1, '1']

// 枚举
enum Gender {
    male,
    female
}

const mine: { name: string, gender: Gender } = {
    name: '彭于晏',
    gender: Gender.male
}

// any
const temp: any = 1

// void
function foo(): void {
    console.log('wo ~ ing')
}

// undefined
const un: undefined = undefined

// null
const nu: null = null

// never
function error(message: string): never {
    throw new Error(message)
}

function loop(): never {
    // 永远不结束
    while (true) {

    }
}

// function
const sum: (x: number, y: number) => number = (x, y) => {
    return x + y
}


// object
declare function create(o: object | null): void;

// 联合类型
const dom: string | number = 1

// 交叉类型
const you: { age: number } & { gender: Gender } = {
    age: 19,
    gender: Gender.female
}

// 类型断言
const unknown: any = 1
// const unKnownLength: number = (<string>unknown).length
const unKnownLength: number = (unknown as string).length

// 类型别名
type clock = Date
const nowTime: clock = new Date()

// 接口
interface Person {
    // 只读
    readonly name: string
    age: number
    // 可选
    hobby?: Array<string>
    // 索引签名
    // [propName: string]: any
}

const cxk: Person = {
    name: 'cxk',
    age: 12,
    hobby: ['唱', '跳', 'rap', '篮球']
}

function sayHello(person: Person): void {
    const {name, age, hobby} = person
    console.log(`hello, my name is ${name}, age is ${age}, I like ${hobby.join(',')}`)
}

sayHello(cxk)

// 只读
const onlyArr: ReadonlyArray<number> = [1, 2, 3]

// 函数类型接口
interface FooFun {
    (name: string): void
}

const sayGoodBye: FooFun = name => {
    console.log(`Good-bye ${name}`)
}

// 索引签名
interface StringOfArray {
    [index: number]: string
}

interface StringOfDictionaries {
    [propName: string]: string
}