interface Part {
    is: number
    name: string
    foo: () => void
}


type TP<T> = {
    [K in keyof T] : T[K] extends Function ? K : never
}[keyof T]


type A = TP<Part>

const a:A = 'foo'
