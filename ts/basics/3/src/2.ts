type TP_INFER<T> = T extends Array<infer U> ? U : T

type B = TP_INFER<string>
type C = TP_INFER<string[]>

const b: B = '1'
const c: C = '1'
