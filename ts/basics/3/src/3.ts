const setHello = <T extends { new(...args: any[]): {} }>(target: T) => {
    return class extends target {
        public hello = 'B'
        public age = 1
    }
}

const setAge = <T extends { new(...args: any[]): {} }>(target: T) => {
    return class extends target {
        public hello = 'C'
        public age = 2
    }
}

const changeGetAge = (flag: boolean) => {
    return (target: Object, pName: string, desc: TypedPropertyDescriptor<() => number>) => {
        console.log(desc, pName, target)
        return {
            value: () => {
                return 123
            },
            enumerable: flag
        }
    }
}

@setAge
@setHello
class D {
    public hello = 'A'
    public age = 0

    constructor() {
    }

    @changeGetAge(true)
    public getAge() {
        return this.age
    }
}

const d = new D()
console.log(d.getAge())
for (let k in d) {
    console.log(k)
}
