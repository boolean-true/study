import superagent = require("superagent");
import cheerio = require("cheerio");

interface bookInfo {
    title: string,
    author: string,
    like: number
}

class Crawler {
    private URL = 'https://www.jianshu.com'

    async getRawHtml() {

    }

    getPageData(html: string) {
        const $ = cheerio.load(html);
        const elements = $('.note-list .content');
        const bookInfo: bookInfo[] = [];
        elements.map((index, item) => {
            const title = $(item).find('.title').text();
            const author = $(item).find('.nickname').text();
            const like = $(item).find('.meta span').eq(1).text();
            bookInfo.push({
                title,
                author,
                like: Number.parseInt(like)
            });
        });

        console.log(bookInfo);
    }

    constructor() {
        superagent.get(this.URL).then(({text}) => {
            this.getPageData(text);
        })
    }
}

const crawler = new Crawler();
