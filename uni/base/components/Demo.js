export default {
	name: 'Demo',
	functional: true,
	data() {
		return {
			message: 'hello'
		}
	},
	render(h, context) {
		console.log(context)
		const {data} = context
		const {message} = data
		return h('h1', {}, [message])
	}
}
