import '@/styles/index.scss'
import {createApp} from 'vue'

import Worker from './worker?worker'

import pkg from '../package.json'
console.log(pkg)

const worker = new Worker()
worker.onmessage = e => {
    console.log(e.data)
}

import App from "./App.vue";

createApp(App).mount('#app')


