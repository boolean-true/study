let count = 1

const timedCount = () => {
    count += 1
    postMessage(count)
    setTimeout(timedCount, 1000)
}

timedCount()
