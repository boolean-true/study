import path from "path";
import {defineConfig} from 'vite'

import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import react from '@vitejs/plugin-react'


export default defineConfig({
    plugins: [
        vue(),
        vueJsx(),
        react()
    ],
    resolve: {
        alias: [
            {find: '@', replacement: path.resolve(__dirname, 'src')}
        ]
    },

})
