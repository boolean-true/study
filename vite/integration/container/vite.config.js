import {join} from 'path'
import {defineConfig} from 'vite'

export default defineConfig({
    build: {
        manifest: true,
        outDir: join(__dirname, '../', 'dist')
    }
})
