const express = require('express')

const app = express()

const manifest = require('./dist/manifest.json')

app.set('view engine', 'pug')
app.use(express.static('dist'))

app.get('/', (req, res) => {
    res.render('index', {
        message: 'hello pug',
        title: 'pug yes',
        data: [
            {id: 1, name: 'one'},
            {id: 2, name: 'two'}
        ],
        index: manifest["index.html"].file
    })
})

app.listen(5000, 'localhost', function () {
    const host = this.address().address
    const port = this.address().port
    console.log(`http://%s:%s`, host, port)
})
