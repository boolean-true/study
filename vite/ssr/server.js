const fs = require('fs')
const path = require('path')
const express = require('express')

const app = express()
const {createServer: createViteServer} = require('vite')

const isDevelopment = process.env.NODE_ENV === 'development'

const port = 4000

const fileToTemplate = (filePath, url, vite) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (error, data) => {
            if (error) return reject(error)
            resolve(vite.transformIndexHtml(url, data))
        })
    })
}

createViteServer({
    server: {middlewareMode: 'ssr'}
}).then(vite => {
    if (isDevelopment) {
        app.use(vite.middlewares)
    } else {
        app.use(express.static(path.resolve(__dirname, 'dist/client'), {index: false}))
    }

    app.get('*', async (req, res) => {
        const {originalUrl: url} = req
        let template = ""
        let enterMain = {}
        let manifest = {}

        if (isDevelopment) {
            template = await fileToTemplate('index.html', url, vite)
            enterMain = await vite.ssrLoadModule('/src/entry-server')
        } else {
            template = (fs.readFileSync('./dist/client/index.html', 'utf-8') || '')
            enterMain = require('./dist/server/entry-server')
            manifest = require('./dist/client/manifest.json')
        }
        const vueApp = enterMain.createApp()
        const htmlMain = await enterMain.render(url, vueApp, manifest)
        const html = template.replace('<!-- APP_HTML -->', htmlMain)
        res.status(200).set({'Content-Type': 'text/html'}).end(html)
    })

    app.listen(port, 'localhost', function () {
        const host = this.address().address
        const port = this.address().port
        console.log(`http://%s:%s`, host, port)
    })
})
