import {renderToString} from '@vue/server-renderer'
import createVueApp from "./main";

export const createApp = () => createVueApp(true)

export const render = async (url, app, manifest) => {
    const ctx = {}
    if (app.config.globalProperties.$router) {
        await app.config.globalProperties.$router.push(url)
        await app.config.globalProperties.$router.isReady()
    }
    const html = await renderToString(app, ctx)
    return html
}

