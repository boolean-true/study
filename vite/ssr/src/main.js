import {createApp, createSSRApp} from 'vue'
import App from './App.vue'
import createVueRouter from './router'

const createVueApp = (ssr) => {
    const app = ssr ? createSSRApp(App) : createApp(App)
    const router = createVueRouter()
    app.use(router)
    return app
}

export default createVueApp
