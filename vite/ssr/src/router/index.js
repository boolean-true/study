import {createRouter, createWebHistory, createMemoryHistory} from 'vue-router'

const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        component: () => import('@/views/home/index.vue')
    },
    {
        path: '/about',
        component: () => import('@/views/about/index.vue')
    }
]

const createVueRouter = () => createRouter({
    routes,
    history: import.meta.env.SSR ? createMemoryHistory() : createWebHistory()
})

export default createVueRouter
