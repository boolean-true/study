const path = require('path')

const resolve = (dir) => {
    return path.join(__dirname, dir)
}

const name = 'admin'

const port = 5000

module.exports = {
    productionSourceMap: false,
    devServer: {
        port,
        overlay: {
            // 全屏提示， 只在报错时候出现
            warnings: false,
            errors: true
        },
        open: true,
        // before:
    }
}
