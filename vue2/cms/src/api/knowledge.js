import service from "@/request"

const baseUrl = `/api/knowledgeMap`

export const newKnowledgeAtlas = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/saveRootKnowledgeMap`
    })
}

export const getKnowledgeAtlas = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/getKnowledgePageInfo`
    })
}

export const removeKnowledge = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/deleteKnowledgeMap`
    })
}

export const getKnowledgeDetail = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/getKnowledgeDetail`
    })
}

export const removeKnowledgeNode = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/deleteKnowledgeMap`
    })
}

export const createKnowledgeNode = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/saveChildKnowledgeMap`
    })
}

export const updateKnowledgeNode = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/updateChildKnowledgeMap`
    })
}

export const uploadKnowledgeNodeFile = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/upload`
    })
}

export const getMediaUrl = (data) => {
    return service({
        data,
        method: 'post',
        url: `${baseUrl}/getKnowledgeUrl`
    })
}
