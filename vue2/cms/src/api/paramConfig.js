import service from "@/request";

export const getParamConfig = (params) => {
    return service({
        params,
        method: 'get',
        url: `/api/oss/paramConfig`
    })
}
