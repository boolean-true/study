const renderItem = (h, context) => {
    const {data, type} = context.props
    const tag = type === 'video' ? type : 'img'
    return data.map(v => {
        const domProps = {src: v}
        const videoProps = {
            controls: true,
            controlsList: 'nodownload',
            disablePictureInPicture: true
        }
        return h('li', {
            domProps: {
                className: 'media-item'
            }
        }, [h(tag, {domProps: Object.assign(domProps, videoProps)})])
    })
}

export default (h, context) => {
    return h('ul', {
        domProps: {
            className: 'media-container'
        }
    }, renderItem(h, context))
}
