const autoFocus = el => {
    if (window.getSelection) { //ie11 10 9 ff safari
        el.focus() //解决ff不获取焦点无法定位问题
        const range = window.getSelection() //创建range
        range.selectAllChildren(el) //range 选择el下所有子内容
        range.collapseToEnd() //光标移至最后
    } else if (document.selection) { //ie10 9 8 7 6 5
        const range = document.selection.createRange() //创建选择对象
        //const range = document.body.createTextRange()
        range.moveToElementText(el) //range定位到el
        range.collapse(false) //光标移至最后
        range.select()
    }
}


export default autoFocus
