import axios from 'axios'
import cookie from "@/utils/cookie";

const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_API
})

service.interceptors.request.use(config => {
    // config.headers.Authorization = sessionStorage.getItem('token')
    return config
})

service.interceptors.response.use(response => {
    const {data} = response
    return data
}, error => {
    console.log(error.message)
    return Promise.reject(error)
})

export default service
