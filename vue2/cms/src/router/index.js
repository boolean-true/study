import Vue from 'vue'
import VueRouter from 'vue-router'
import modules from "./modules"

Vue.use(VueRouter)

export const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        component: () => import('@/views/Login')
    },
    ...modules
]

const router = new VueRouter({
    scrollBehavior: () => ({y: 0}),
    routes
})

export default router
