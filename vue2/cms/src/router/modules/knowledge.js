import Layout from '@/components/Layout'

const route = {
    path: '/knowledge',
    component: Layout,
    redirect: '/knowledge/index',
    menu: true,
    children: [
        {
            path: 'index',
            component: () => import('@/views/Knowledge'),
            name: 'KnowledgeIndex',
            menu: true,
            meta: {
                title: '知识图谱配置',
                icon: 'el-icon-setting',
            }
        },
        {
            path: 'config',
            component: () => import('@/views/Knowledge/config'),
            name: 'KnowledgeConfig',
            meta: {
                title: '配置',
                activeMenu: '/knowledge/index'
            }
        }
    ]
}
export default route
