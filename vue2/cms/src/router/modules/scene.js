import Layout from '@/components/Layout'

const route = {
    path: '/scene',
    component: Layout,
    redirect: '/scene/index',
    menu: true,
    children: [
        {
            path: 'index',
            component: () => import('@/views/Scene'),
            name: 'SceneIndex',
            menu: true,
            meta: {
                title: '场景配置',
                icon: 'el-icon-s-comment',
            }
        }
    ]
}
export default route
