import {getParamConfig} from "@/api/paramConfig";

const state = {
    config: {}
}

const mutations = {
    SET_CONFIG(state, payload) {
        state.config = payload
    }
}

const actions = {
    getParamConfig({commit}) {
        return new Promise((resolve, reject) => {
            const paramConfig = JSON.parse(sessionStorage.getItem('paramConfig'))
            if (paramConfig) {
                commit('SET_CONFIG', paramConfig)
                return resolve(paramConfig)
            }
            getParamConfig().then(data => {
                const {result, retData, retMsg} = data
                if (result !== 'SUCCESS') return reject(retMsg)
                const {bucketName: bucket, endpoint, region, type} = retData || {}
                const config = {
                    bucket,
                    endpoint,
                    region,
                    type
                }
                sessionStorage.setItem('paramConfig', JSON.stringify(config))
                commit('SET_CONFIG', config)
            }).catch(e => reject(e))
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
