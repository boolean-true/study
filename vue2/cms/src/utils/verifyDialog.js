import {MessageBox} from 'element-ui'
/*
* 二次封装confirm, 简化代码量
* */
export default function (message = '你确定要删除吗？ 是否继续？', type = 'warning') {
    return new Promise((resolve, reject) => {
        MessageBox.confirm(message, '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type
        }).then(() => {
            resolve(true)
        }).catch(Function.prototype)
    })
}
