import Layout from '@/components/Layout'

const route = {
    path: '/charts',
    component: Layout,
    redirect: 'noRedirect',
    name: 'Charts',
    meta: {
        title: 'Charts',
        icon: 'chart'
    },
    menu: true,
    children: [
        {
            path: 'keyboard',
            menu: true,
            component: () => import('@/views/Charts/keyboard'),
            name: 'KeyboardChart',
            meta: { title: 'Keyboard Chart', noCache: true }
        },
        {
            path: 'line',
            menu: true,
            component: () => import('@/views/Charts/line'),
            name: 'LineChart',
            meta: { title: 'Line Chart', noCache: true }
        }
    ]
}

export default route
