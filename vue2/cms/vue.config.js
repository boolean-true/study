'use strict'
const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    configureWebpack: {
        resolve: {
            alias: {
                '@': resolve('src')
            }
        }
    },
    // 配置scss入口
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/styles/index.scss";`
            }
        }
    }
}
