export default {
    debounce(fun, duration = 0) {
        let timer = null;
        return function (...arg) {
            clearTimeout(timer);
            timer = setTimeout(() => {
                fun.apply(this, arg)
            }, duration)
        }
    },

    throttle(fun, duration = 0) {
        let timer = null;
        return function (...args) {
            if (!timer) {
                timer = setTimeout(() => {
                    timer = null;
                    fun.apply(this, args);
                }, duration);
            }
        }
    }
}