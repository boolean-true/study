import NavBarComponent from './NavBar.vue';
const NavBar = {
    install(Vue) {
        Vue.component('navBar', NavBarComponent)
    }
}

export default NavBar