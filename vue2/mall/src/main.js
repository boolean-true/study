import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import './network/axios';
import plugins from './plugins'

plugins(Vue);

import utils from '@assets/js/utils'

Vue.prototype.$utils = utils;

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
