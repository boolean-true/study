import Vue from 'vue';
import axios from 'axios';
import config from './config';

const instance = axios.create({
    baseURL: config.baseURL,
    timeout: config.timeout
});

// 请求拦截
instance.interceptors.request.use(config => {
    return config;
});

// 响应拦截
instance.interceptors.response.use(result => {
    return result.data;
});

Vue.prototype.$axios = instance;