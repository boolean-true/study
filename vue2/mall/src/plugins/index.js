import NavBar from '@components/NavBar'; // 

import MainView from '@components/MainView/MainView.vue';


import ScrollView from '@components/ScrollView/ScrollView.vue';


// 轮播图
import VueAwesomeSwiper from 'vue-awesome-swiper';
import 'swiper/css/swiper.css'

import ScrollBar from "@components/ScrollBar/ScrollBar.vue";
import Waterfall from "@components/Waterfall/Waterfall.vue";
import TransitionView from "@components/TransitionView/TransitionView.vue";

export default function plugins(Vue) {
    Vue.use(VueAwesomeSwiper);
    Vue.use(NavBar);

    Vue.component('mainView', MainView);
    Vue.component('scrollView', ScrollView);
    Vue.component('scrollBar', ScrollBar);
    Vue.component('waterfall', Waterfall);
    Vue.component('transitionView', TransitionView);
}