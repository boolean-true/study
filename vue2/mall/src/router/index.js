/*jshint esversion: 6 */
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('@views/Home/index.vue'),
  },
  {
    path: '/category',
    name: 'category',
    component: () => import('@views/Category/index.vue')
  },
  {
    path: '/cart',
    name: 'cart',
    component: () => import('@views/Cart/index.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('@views/Profile/index.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router