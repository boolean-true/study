module.exports = {
    configureWebpack: {
        resolve: {
            // 配置别名,修改后需要重新编译才能生效
            alias: {
                '@assets': '@/assets',
                '@components': '@/components',
                '@views': '@/views',
            }
        }
    },
    // dev服务 
    devServer: {
        open: true,
        proxy: {
            '/api': {
                target: 'http://152.136.185.210:8000/api/w6',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    },
    // 配置scss入口
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@assets/css/function/index.scss";`
            }
        }
    }
}