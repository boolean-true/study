import Vue from 'vue';
import elementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';
import router from './router';
import store from './store';
import './network/axios';

import DInput from './lib'

Vue.use(DInput)

import move from '@/components/move'

Vue.config.productionTip = false

Vue.use(move)
//

Vue.use(elementUI)
// 在项目里引用：
// 在项目里引用：
import Vue2OrgTree from 'vue2-org-tree'
import 'vue2-org-tree/dist/style.css'
import './registerServiceWorker'
Vue.use(Vue2OrgTree)

const app = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
