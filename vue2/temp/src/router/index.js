import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'), // 路由懒加载
    children: [
      {
        path: '/',
        redirect: 'news'
      },
      {
        path: 'news',
        component: () => import('../views/News.vue')
      },
      {
        path: 'table',
        component: () => import('../views/Table.vue')
      }
    ]
  },
  {
    path: '/demo/:username', // 动态路由
    name: 'Demo',
    component: () => import('../views/Demo.vue')
  },
  {
    path: '/pro_file',
    name: 'pro_file',
    component: () => import('../views/ProFile.vue')
  },
  {
    path: '/other/:userKey',
    name: 'other',
    component: () => import('../views/Other.vue'),
    props: true, // 开启props传参
    meta: {
      title: 'other'
    }
  },
  {
    path: '/audio',
    name: 'audio',
    component: () => import('../views/Audio.vue'),
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/vuex',
    name: 'vuex',
    component: () => import('../views/Vuex.vue')
  },
  {
    path: '/sing',
    name: 'sing',
    component: () => import('../views/Sing.vue')
  },
  {
    path: '/axios',
    name: 'axios',
    component: () => import('../views/Axios.vue')
  },
  {
    path: '/wechat',
    name: 'wechat',
    component: () => import('../views/Wechat.vue')
  },
  {
    path: '/test',
    name: 'test',
    component: () => import('../views/Test.vue')
  },
  {
    path: '/move',
    name: 'move',
    component: () => import('../views/Move.vue')
  },
  {
    path: '/md',
    name: 'md',
    component: () => import('../views/Md.vue')
  },
  {
    path: '/picker',
    name: 'picker',
    component: () => import('../views/Picker.vue')
  },
  {
    path: '/tree',
    name: 'Tree',
    component: () => import('../views/Tree.vue')
  },
  {
    path: '/render',
    name: 'Render',
    component: () => import('../views/Render.vue')
  },
  {
    path: '/renderer',
    name: 'Renderer',
    component: () => import('../views/Renderer')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import('../views/Table')
  },
  {
    path: '/editor',
    name: 'Editor',
    component: () => import('../views/Editor')
  }
]

const router = new VueRouter({
  routes,
  base: process.env.NODE_ENV === 'production' ? '/pwa' : process.env.BASE_URL,
  mode: 'history' // 路由模式 // 在router-link中怎加replace属性可以使得history的pushState更改为replaceState，使得浏览器的回退按钮不能后退
})

export default router
