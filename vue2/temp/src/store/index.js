import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    countSum(state) {
      state.count++;
    },
    countSub(state) {
      state.count--;
    },
    countUp(state, payload) {
      state.count += payload;
    },
    countDec(state, payload) {
      state.count -= payload;
    }
  },
  getters: {
    countSquare(state) {
      return Math.pow(state.count, 2);
    },
    countPower(state) {
      return (power) => {
        return Math.pow(state.count, power);
      }
    }
  },
  actions: {
    // 异步操作
    timerCountAdd(context) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          // 异步操作的内容
          resolve();
        }, 1000);
      })
    },
    timerCountSub(context, payload) {
      setTimeout(() => {
        context.commit("countDec", payload.data.num);
        payload.success(); // 成功后的回调
      }, 1000);
    }
  },
  modules: {
  }
})
