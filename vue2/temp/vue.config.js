const MonacoEditorPlugin = require('monaco-editor-webpack-plugin')

module.exports = {
    configureWebpack: (config) => {
        config.module.rules.push({
            test: /\.lrc$/,
            use: 'raw-loader'
        })
    },

    pwa: {
        workboxPluginMode: 'GenerateSW',
        workboxOptions: {
            navigateFallback: '/index.html',
            runtimeCaching: [
                {
                    urlPattern: new RegExp('^https://api.zippopotam.us/us/'),
                    handler: 'networkFirst',
                    options: {
                        networkTimeoutSeconds: 20,
                        cacheName: 'api-cache',
                        cacheableResponse: {
                            statuses: [0, 200]
                        }
                    }
                }
            ]
        }
    }
}