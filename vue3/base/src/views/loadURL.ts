import axios from 'axios'
import {ref} from 'vue'

function loadURL<T>(url: string) {
    const result = ref<T | null>(null)
    const error = ref()
    const loading = ref()

    axios.get(url).then(({data}) => result.value = data)
        .catch(e => error.value = e.message)
        .finally(() => loading.value = false)

    return {
        result,
        error,
        loading
    }
}

export default loadURL