import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'
const modules = import.meta.globEager('./modules/**/*.ts')

const routeModule: RouteRecordRaw[] = []

Object.keys(modules).forEach((key) => {
    const mod = modules[key].default || {}
    const modList = Array.isArray(mod) ? [...mod] : [mod]
    routeModule.push(...modList)
});

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: '/home'
    },
    ...routeModule
]

const router = createRouter({
    history: createWebHistory(),
    routes
})


export default router
