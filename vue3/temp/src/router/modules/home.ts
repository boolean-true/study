import {RouteRecordRaw} from 'vue-router'

const homeRoute: RouteRecordRaw[] = [
    {
        path: '/home',
        component: () => import('@/views/home/index.vue')
    }
]

export default homeRoute
