import {RouteRecordRaw} from 'vue-router'

const imageRoute: RouteRecordRaw[] = [
    {
        path: '/image',
        component: () => import('@/views/image/index.vue')
    }
]

export default imageRoute
