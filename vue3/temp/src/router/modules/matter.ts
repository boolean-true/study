import {RouteRecordRaw} from 'vue-router'

const matterRoute: RouteRecordRaw[] = [
    {
        path: '/matter',
        component: () => import('@/views/matter/index.vue')
    }
]

export default matterRoute
