import {RouteRecordRaw} from 'vue-router'

const screenRoute: RouteRecordRaw[] = [
    {
        path: '/screen',
        component: () => import('@/views/screen/index.vue')
    }
]

export default screenRoute
