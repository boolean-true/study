import {RouteRecordRaw} from 'vue-router'

const tempRoute: RouteRecordRaw[] = [
    {
        path: '/temp',
        component: () => import('@/views/temp/index.vue')
    }
]

export default tempRoute
