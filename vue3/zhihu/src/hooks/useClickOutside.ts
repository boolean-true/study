import {ref, Ref, onUnmounted, onMounted} from 'vue'

const useClickOutside = (ele: Ref<null | HTMLElement>): Ref<boolean> => {
    const isOutside = ref<boolean>(false)
    const handler = (e: MouseEvent): void => {
        if (!ele || !ele.value) return
        isOutside.value = !ele.value.contains(e.target as HTMLElement)
    }

    onMounted(() => {
        document.addEventListener('click', handler)
    })

    onUnmounted(() => {
        document.removeEventListener('click', handler)
    })

    return isOutside
}
export default useClickOutside