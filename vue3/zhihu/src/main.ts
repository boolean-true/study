import {createApp} from 'vue'
import {createRouter, createWebHistory} from 'vue-router'
import {createStore} from "vuex"

import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/styles/index.css'

import App from './App.vue'
import Layout from '@/components/Layout/index.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: '/home',
            component: Layout,
            children: [
                {
                    path: 'home',
                    name: 'home',
                    component: () => import('@/views/home/index.vue'),
                }
            ]
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('@/views/login/index.vue')
        }
    ]
})


const store = createStore({
    state: {}
})

createApp(App)
    .use(router)
    .use(store)
    .mount('#app')
