// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云数据库
const db = cloud.database()

// 连接库
const dbCollection = db.collection('playlist');

// http请求工具
const $http = require('request-promise');

// 网易热门推荐的歌单
const BASE_URL = 'http://192.168.3.173:3000';

// 最大查询条数
const MAX_LIMIT = 100;

// 云函数入口函数
exports.main = async (event, context) => {
  // 库中所有的歌单
  // const { data: dbPlayList } = await dbCollection.get(); // 有弊端,最多只能查询100条数据
  const { total: dbPlayListTotal } = await dbCollection.count(); // 总条数
  const queryNum = Math.ceil(dbPlayListTotal / MAX_LIMIT); // 需要查询的次数
  const tasks = []; // 查询队列
  const dbPlayList = []; // 查询到的所有数据集合
  for (let i = 0; i < queryNum; i++) {
    const segment = dbCollection.skip(i * MAX_LIMIT).limit(MAX_LIMIT).get();
    tasks.push(segment);
  }
  const tempPlayList = (await Promise.all(tasks)).map(item => item.data);
  tempPlayList.forEach(item => { dbPlayList.push(...item) });
  // 库中的歌单的id
  const dbPlayListIDs = dbPlayList.map(item => item.id);
  // 远程获取的歌单
  const playList = await $http(`${BASE_URL}/personalized`).then(res => {
    const data = JSON.parse(res).result
    console.log('playList', data)
    return data;
  });

  // 去重,避免重复添加数据库
  const newPlayList = playList.filter(item => !dbPlayListIDs.includes(item.id));
  newPlayList.forEach(async item => {
    await dbCollection.add({
      data: {
        ...item,
        createTime: db.serverDate()
      }
    })
  })

  return newPlayList.length;
}