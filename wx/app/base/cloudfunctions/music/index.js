// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
const $http = require('request-promise')

cloud.init()

const db = cloud.database();

const dbPlaylistCollection = db.collection('playlist');

const BASE_URL = 'http://192.168.3.173:3000';

// 云函数入口函数
exports.main = async (event, context) => {
  const app = new TcbRouter({ event });

  app.router('playlist', async (ctx, next) => {
    ctx.body = await $http(`${BASE_URL}/personalized`).then(res => {
      return JSON.parse(res).result
    })
    // const { start, limit } = event;
    // ctx.body = await dbPlaylistCollection
    //   .skip(start)
    //   .limit(limit)
    //   // .orderBy('createTime', 'desc')
    //   .get()
    //   .then(result => result.data);
  });

  app.router('musiclist', async (ctx, next) => {
    const { playListID } = event;
    ctx.body = await $http(`${BASE_URL}/playlist/detail?id=${playListID}`).then(res => {
      const data = JSON.parse(res).playlist
      console.log('musiclist', data)
      return data
    })
  });

  app.router('musicURL', async (ctx, next) => {
    const { musicID } = event;
    ctx.body = await $http(`${BASE_URL}/song/url?id=${musicID}`).then(res => {
      const data = JSON.parse(res)
      console.log('musicURL', data)
      return data
    })
  });

  return app.serve()
}