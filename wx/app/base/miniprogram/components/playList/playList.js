// components/songListItem/songListItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    songData: {
      type: Object
    }
  },

  observers: {
    ['songData.playCount'](val) {
      this.setData({
        _playCount: this._conversion(val, 2)
      });
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    _playCount: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    _conversion(num, toFixed) {
      const numStr = Number.parseInt(num).toString();
      const leng = numStr.length;

      if(leng < 6) {
        return num
      } else if(leng >= 6 && leng <= 8) {
        const dotNum = numStr.substring(leng - 4, leng - 4 + toFixed);
        return `${Number.parseInt(num / 10000)}.${dotNum}万`
      } else if(leng > 8) {
        const dotNum = numStr.substring(leng - 8, leng - 8 + toFixed);
        return `${Number.parseInt(num / 100000000)}.${dotNum}亿`
      }
    }
  }
})
