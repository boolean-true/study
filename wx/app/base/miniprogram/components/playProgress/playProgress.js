
const backgroundAudioManager = wx.getBackgroundAudioManager();
let [movableAreaWidth, movableViewWidth] = [0, 0];
let currentSec = -1;
let touchX = 0;
let isTouch = false;
let musicDuration = 0;
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    currentTime: '00:00',
    totalTime: '00:00',
    moveableX: 0,
    progress: 0
  },
  lifetimes: {
    ready() {
      this._getMoveableX();
      this._bindMusciEvent();
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    _getMoveableX() {
      const query = this.createSelectorQuery();
      query.select('.movable-area').boundingClientRect();
      query.select('.movable-view').boundingClientRect();
      query.exec((rect) => {
        const [{ width: areaWidth }, { width: viewWidth }] = rect;
        movableAreaWidth = areaWidth;
        movableViewWidth = viewWidth;
      });
    },

    _bindMusciEvent() {
      backgroundAudioManager.onPlay(() => {

      });
      backgroundAudioManager.onPause(() => {

      });
      backgroundAudioManager.onStop(() => {

      });
      backgroundAudioManager.onWaiting(() => {

      });
      backgroundAudioManager.onCanplay(() => {
        const { duration } = backgroundAudioManager;
        if (typeof duration !== 'undefined') {
          musicDuration = duration
          this._setTime(duration, 'totalTime')
        } else {
          setTimeout(() => {
            musicDuration = backgroundAudioManager.duration
            this._setTime(musicDuration, 'totalTime')
          }, 1000)
        }
      });
      backgroundAudioManager.onTimeUpdate(() => {
        if (isTouch) { return false }
        const { currentTime } = backgroundAudioManager
        const sec = Number.parseInt(currentTime); // 避免频繁setData
        if (currentSec !== sec) {
          this._setTime(currentTime, 'currentTime');
          const ratio = currentTime / musicDuration;
          this.setData({
            moveableX: (movableAreaWidth - movableViewWidth) * ratio,
            progress: ratio * 100
          });
          currentSec = sec;
        }
      });
      backgroundAudioManager.onEnded(() => {

      });
      backgroundAudioManager.onStop(() => {

      });
      backgroundAudioManager.onEnded(() => {
      });
      backgroundAudioManager.onSeeked(() => {
        console.log(1)
      });
    },

    _setTime(duration, time) {
      const minute = Math.floor(duration / 60).toString().padStart(2, '0');
      const second = Math.floor(duration % 60).toString().padStart(2, '0');

      this.setData({
        [time]: `${minute}:${second}`
      })
    },

    onMoveChange(e) {
      const { x, source } = e.detail;
      if (source === 'touch') {
        touchX = x;
      }
    },
    onMoveTouchStart(e) {
      isTouch = true;
    },
    onMoveTouchEnd(e) {
      const progress = touchX / (movableAreaWidth - movableViewWidth) * 100;
      const touchTime = musicDuration * progress / 100;
      backgroundAudioManager.seek(touchTime);
      this.setData({
        moveableX: touchX,
        progress: progress
      });
      this._setTime(touchTime, 'currentTime');
      wx.nextTick(() => {
        isTouch = false;
      })
    },
  }
})
