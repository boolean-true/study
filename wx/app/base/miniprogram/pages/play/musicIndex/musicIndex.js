// pages/play/musicIndex/musicIndex.js
const backgroundAudioManager = wx.getBackgroundAudioManager();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    musiclist: [],
    coverImage: '',
    coverName: '',
    description: '',
    activeMusicID: -1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._getMusicList(options.playListID);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  _getMusicList(playListID) {
    wx.showLoading({
      title: 'loading',
    });
    wx.cloud.callFunction({
      name: 'music',
      data: {
        playListID,
        $url: 'musiclist',
      }
    }).then(res => {
      const { tracks, description, name, coverImgUrl } = res.result;
      this.setData({
        description,
        musiclist: tracks,
        coverImage: coverImgUrl,
        coverName: name
      });
      wx.setStorageSync('musiclist', tracks);
    }).finally(() => {
      wx.hideLoading();
    })
  },

  chooseMusic(e) {
    backgroundAudioManager.stop();
    const { id, index } = e.currentTarget.dataset;
    wx.setStorageSync('musiclist', this.data.musiclist);
    this.setData({
      activeMusicID: id
    });
    wx.navigateTo({
      url: `/pages/play/player/player?musicID=${id}&index=${index}`
    })
  }
})