
// 最大获取条数
const MAX_LIMIT = 15;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    wiperImages: [
      {
        id: 1,
        url: 'http://p1.music.126.net/oeH9rlBAj3UNkhOmfog8Hw==/109951164169407335.jpg',
      },
      {
        id: 2,
        url: 'http://p1.music.126.net/xhWAaHI-SIYP8ZMzL9NOqg==/109951164167032995.jpg',
      },
      {
        id: 3,
        url: 'http://p1.music.126.net/Yo-FjrJTQ9clkDkuUCTtUg==/109951164169441928.jpg',
      }
    ],
    playList: [],
    isLoadFinish: false // 歌单列表是否加载完成
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._getPlayList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      playList: [],
      isLoadFinish: false
    });
    this._getPlayList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // this._getPlayList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 分页获取歌单列表 
   */
  _getPlayList() {
    const { playList, isLoadFinish } = this.data;
    if (isLoadFinish) { return };
    wx.showLoading({
      title: 'loading',
    });
    console.log({
      $url: 'playlist',
      start: playList.length,
      limit: MAX_LIMIT
    })
    wx.cloud.callFunction({
      name: 'music',
      data: {
        $url: 'playlist',
        start: playList.length,
        limit: MAX_LIMIT
      }
    }).then(res => {
      const { result } = res;
      this.setData({
        playList: playList.concat(result),
        isLoadFinish: result.length < MAX_LIMIT
      });
      wx.stopPullDownRefresh(); // 下拉关闭
    }).finally(() => {
      wx.hideLoading();
    })
  },

  // 去歌曲列表页
  toMusicList(e) {
    const { id } = e.currentTarget.dataset;
    wx.navigateTo({
      url: `/pages/play/musicIndex/musicIndex?playListID=${id}`
    })
  }
})