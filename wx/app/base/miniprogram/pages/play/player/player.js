
// 定义歌曲列表, 当前的音乐id，当前的音乐索引
let [musiclist, musicData, activeMusicID, activeMusicIndex] = [[], {}, 0, 0];
const backgroundAudioManager = wx.getBackgroundAudioManager();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    musicCover: '',
    playing: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._init(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  _init(options) {
    const { index, musicID } = options;
    musiclist = wx.getStorageSync('musiclist');
    activeMusicID = musicID;
    activeMusicIndex = index;
    musicData = musiclist[index];
    this._getMusic();
  },

  _getMusic() {
    backgroundAudioManager.stop();
    this.setData({
      playing: false
    });
    wx.setNavigationBarTitle({
      title: musicData.name
    });
    this.setData({
      musicCover: musicData.al.picUrl
    });
    wx.cloud.callFunction({
      name: 'music',
      data: {
        $url: 'musicURL',
        musicID: activeMusicID
      }
    }).then(res => {
      const [music] = res.result.data;
      const { name, al, ar } = musicData;
      let singer = '';

      this.setData({
        playing: true
      });
      backgroundAudioManager.src = music.url;
      backgroundAudioManager.title = name;
      backgroundAudioManager.coverImgUrl = al.picUrl;
      backgroundAudioManager.singer = singer;
      backgroundAudioManager.epname = al.name;
      ar.forEach((item, index) => {
        singer += `${item.name}${index === ar.length - 1 ? '' : '，'}`;
      });
      backgroundAudioManager.onStop(() => {
        this.setData({
          playing: false
        });
      });

      backgroundAudioManager.onPause(() => {
        this.setData({
          playing: false
        });
      });

      backgroundAudioManager.onPlay(() => {
        this.setData({
          playing: true
        });
      });

      backgroundAudioManager.onEnded(() => {
        this.onNextMusic();
      });
    })
  },
  /*
   * 播放暂停
   */
  changePlaying() {
    const { playing } = this.data;
    if (playing) {
      backgroundAudioManager.pause();
    } else {
      backgroundAudioManager.play();
    }
    this.setData({
      playing: !playing
    })
  },

  /*
   * 下一首
   */
  onNextMusic() {
    if (activeMusicIndex === musiclist.length - 1) {
      activeMusicIndex = 0;
    } else {
      activeMusicIndex++;
    }
    musicData = musiclist[activeMusicIndex];
    activeMusicID = musicData.id;
    this._getMusic();
  },
  /*
   * 上一首
   */
  onPreviouMusic() {
    if (activeMusicIndex) {
      activeMusicIndex--;
    } else {
      activeMusicIndex = musiclist.length - 1;
    }
    musicData = musiclist[activeMusicIndex];
    activeMusicID = musicData.id;
    this._getMusic();
  }
})