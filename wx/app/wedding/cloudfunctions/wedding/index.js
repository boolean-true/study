// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV
})
cloud.init()

const db = cloud.database();
const weddingDbCollection = db.collection('wedding')
const weddingDbKey = '001'

// 云函数入口函数
exports.main = async (event, context) => {
    const rawData = await weddingDbCollection.doc(weddingDbKey).get()
    return rawData
}
