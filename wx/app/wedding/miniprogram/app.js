// app.js
App({
    onLaunch: function () {
        wx.cloud.init({
            env: 'base-6gicjtlze2332d03',
            traceUser: true,
        })
        this.globalData = {
            baseInfo: {}
        }
        wx.getBackgroundFetchData({
            fetchType: 'pre',
            success: (data) => {
                const rawData = JSON.parse(data.fetchedData)
                this.globalData.baseInfo = rawData.data
            }
        })

        // 全局音乐
        const backgroundAudioManager = wx.getBackgroundAudioManager();
        this.globalData.bgMusic = backgroundAudioManager;
        this.globalData.bgMusicPlaying = true;
    },
    onShow() {
        !this.globalData.bgMusicPlaying && this.globalData.bgMusic.play();
        this.globalData.bgMusicPlaying = true;
    },

    onHide() {
        this.globalData.bgMusicPlaying && this.globalData.bgMusic.pause();
        this.globalData.bgMusicPlaying = false;
    },
    // 获取基本的全局信息
    async getBaseInfo() {
        try {
            const db = wx.cloud.database({
                env: 'base-6gicjtlze2332d03'
            })
            const {
                data
            } = await db.collection("wedding").doc("001").get()
            return data
        } catch(e) {
            console.log(e, 111111111111111)

            // do catch
            console.log("error")
        }
    }
})
