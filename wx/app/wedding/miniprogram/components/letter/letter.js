// components/letter/letter.js
const app = getApp();

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        show: {
            type: Boolean,
            value: false
        },
        cover: {
            type: String,
            value: ''
        },
        groom: {
            type: String,
            value: ''
        },
        bride: {
            type: String,
            value: ''
        },
        address: {
            type: String,
            value: ''
        },
        time: {
            type: String,
            value: ''
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        open: false
    },

    /**
     * 组件的方法列表
     */
    methods: {
        handleCloseLetter() {
            this.setData({
                open: false
            })
            setTimeout(() => {
                this.triggerEvent('onClose');
            }, 600)
        },
        handleToHome() {
            wx.switchTab({
                url: '/pages/invitation/invitation'
            })
        },
        handleOpenLetter() {
            this.setData({open: true})
        }
    },
    lifetimes: {
        async attached() {
            if(!this.baseinfo) {
                wx.showLoading();
                const res = await app.getBaseInfo();
                console.log(res)
                this.setData({
                    cover: res.letterCover,
                    groom: res.groom,
                    bride: res.bride,
                    address: res.address,
                    time: res.date
                })
                wx.hideLoading();
                app.globalData.baseinfo = res;
            }
        }
    }
})
