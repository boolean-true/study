const app = getApp();

Component({
  data: {
    musicimg: "/images/common/music_icon.png",
    isplay: true,
    bgMusic: '',
    baseInfo: {},
  },


  methods: {
    // 控制背景音乐
    ctrlMusic() {
      // 播放
      const {
        bgMusicPlaying
      } = app.globalData;

      if (!this.data.isplay) {
        this.setData({
          isplay: true,
        });
        this.data.bgMusic.play();
        app.globalData.bgMusicPlaying = true;
        console.log("music playing !");
        // 结束时循环
        this.data.bgMusic.onEnded(() => {
          console.log("music end !");
          this.setData({
            isplay: true,
          });
          console.log("music replay !");
          this.ctrlMusic();
        });
      } else {
        this.setData({
          isplay: false,
        });
        this.data.bgMusic.pause();
        app.globalData.bgMusicPlaying = false;
        this.data.bgMusic.onPause(() => {
          console.log("music stop !");
        });
      }
    },
  },

  lifetimes: {
    attached() {
      const {
        bgMusic,
        bgMusicPlaying,
        baseInfo,
      } = app.globalData;
      this.setData({
        bgMusic: bgMusic,
        isplay: bgMusicPlaying,
        baseInfo: baseInfo,
      })
      bgMusic.title = this.data.baseInfo.media.title;
      bgMusic.singer = this.data.baseInfo.media.singer;
      this.data.bgMusic.src = this.data.baseInfo.media.url;
    },
  },

  pageLifetimes: {
    show() {
      const {bgMusicPlaying} = app.globalData;
      this.setData({
        isplay: bgMusicPlaying
      })
    },
  }
});