const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseInfo: {},
    markers: [],
    dateTime: ""
  },

  markertap() {
    const {
      baseInfo
    } = app.globalData
    const {
      lat,
      lon,
      address
    } = baseInfo
    console.log(lat, lon, address)
    wx.openLocation({
      latitude: Number.parseFloat(lat),
      longitude: Number.parseFloat(lon),
      name: address,
      address: address
    })
  },

  endCountTimeFun() {
    console.log("end count down")
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const {
      baseInfo
    } = app.globalData
    this.setData({
      baseInfo,
      markers: [{
        id: 1,
        width: "35rpx",
        height: "50rpx",
        latitude: baseInfo.lat,
        longitude: baseInfo.lon,
        callout: {
          content: baseInfo.address,
          padding: "10rpx",
          color: "#eccb90",
          borderRadius: "5rpx",
          bgColor: "#242424",
          textAlign: "center",
          display: "ALWAYS"
        }
      }],
      dateTime: baseInfo.date
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '诚邀参加我的婚礼',
      path: '/pages/index/index',
      imageUrl: this.data.baseInfo.shareCoverImg
    }
  },

  onShareTimeLine() {
    return {
      title: '诚邀参加我的婚礼',
      imageUrl: this.data.baseInfo.shareCoverImg
    }
  }
})